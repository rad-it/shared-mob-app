// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'HomePage';
export const Tab2Root = 'EntityPage';
export const Tab3Root = 'SettingsPage';
export const Tab4Root = "NotificationPage"

//Pages
export const RestaurantDetails = 'RestaurantDetailsPage';
export const MyBookings = 'MyBookingsPage';
export const MenuDetail = 'MenuDetailPage';
export const BookingDialog = 'BookingDialogPage';
export const Notification = 'Nofication';
export const BookingDetail = 'BookingDetailPage';
export const Settings = 'SettingsPage';
