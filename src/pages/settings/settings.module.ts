import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { Principal } from '../../providers/auth/principal.service';
import { SettingsPage } from './settings';

//Services
import { LoginService } from '../../providers/login/login.service';

@NgModule({
  declarations: [
    SettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPage),
    TranslateModule.forChild()
  ],
  exports: [
    SettingsPage
  ],
  providers: [Principal, LoginService]
})
export class SettingsPageModule { }
