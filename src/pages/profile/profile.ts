import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, LoadingController, ActionSheetController, Loading } from 'ionic-angular';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MapsAPILoader } from '@agm/core';

import { NationalityService } from '../entities/nationality/nationality.provider';
import { ClientService } from '../../providers/entities/client.provider';
import { PictureService } from '../entities/picture/picture.provider';
import { AccountService } from '../../providers/auth/account.service';

import { Client } from '../../models/entities/client.model';
import { Picture } from '../../models/entities/picture.model';
import { Settings } from '../pages'

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public person: {id: number, email: string, langKey: string};
  client: Client;
  nationalities: {id: number, nameSp?: string, nameEn: string};
  picture: Picture;
  address: string;
  profileImg: string;
  pictureChange: boolean;
  spinnerLoad: boolean;

  lastImage: string = null;
  loading: Loading;
  // Our local settings object
  options: any;
  myphoto: any;
  latitude: number;
  longitude: number;
  settingsReady = false;
  profileSettings = {
    page: 'profile',
    pageTitleKey: 'SETTINGS_PAGE_PROFILE'
  };

  page: string = 'profile';
  pageTitleKey: string = 'SETTINGS_PAGE_PROFILE';
  pageTitle: string;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    public imagePicker: ImagePicker,
    public translate: TranslateService,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private accountService: AccountService,
    private clientService: ClientService,
    private nationalityService: NationalityService,
    private pictureService: PictureService,
    private camera: Camera,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
  ) {
    this.person = { id: undefined, email: undefined, langKey: undefined};
    this.client = {};
    this.picture = {};
    this.pictureChange = false;
    this.spinnerLoad = true;
  }
  /**
   * Listen for change language
   */
  segmentChanged() {
    this.translate.use(this.person.langKey);
  }

  ionViewDidLoad() {
    var validData = this.loadAll();
    let personItem = JSON.parse(localStorage.getItem('PERSON'));
    let clientItem = JSON.parse(localStorage.getItem('CLIENT'));
    if (!validData) {
      if (clientItem && personItem){
        this.person = personItem;
        this.client = clientItem;
      }
    }
  }

  /**
   * Load all user data
   * @param refresher
   */
  loadAll(refresher?) {
    //Get for account details
    this.accountService.get().subscribe(
      (accountData) => {
        this.person = accountData;
        this.translate.use(this.person.langKey);
        if (accountData) {
          this.clientService.findByUser(accountData.id).subscribe(
            (clientData) => {
              this.client = clientData;
			  this.address = clientData.location? clientData.location.formattedAddress: '';
              this.latitude = clientData.location? clientData.location.latitude: null;
              this.longitude = clientData.location? clientData.location.longitude: null;
              if (this.client.picture && this.client.picture.content) {
			    this.profileImg = this.pathForImage(this.client.picture.content);
              } else {
                this.profileImg = '../../assets/imgs/profile.jpg';
              }
              this.spinnerLoad = false;
              if (typeof(refresher) !== 'undefined') {
                refresher.complete();
              }
              //load Places Autocomplete
              this.mapsAPILoader.load().then(() => {
                let nativeHomeInputBox = document.getElementById('location').getElementsByTagName('input')[0];
                let autocomplete = new google.maps.places.Autocomplete(nativeHomeInputBox, {
                  types: ["address"]
                });
                autocomplete.addListener("place_changed", () => {
                  this.ngZone.run(() => {
                    //get the place result
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    //verify result
                    if (place.geometry === undefined || place.geometry === null) {
                      return;
                    }
                    //set latitude, longitude
                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                  });
                });
              });
              return true;
            },
            (error) => {
              let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
              toast.present();
              return false;
            }
          );
        }
      },
      (error) => {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
        return false;
      });
      //Load nationalities
      this.nationalityService.getAll().subscribe(
        (nationalitiesData) => {
          this.nationalities = nationalitiesData;
        }
      )
    }

  cancel(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.navCtrl && this.navCtrl.pop(null, null);
  }
  /**
   * Save all data from profile
   */
  save() {
    if (this.client && this.person) {
      localStorage.setItem('PERSON', JSON.stringify(this.person));
      localStorage.setItem('CLIENT', JSON.stringify(this.person));
      var clientUpdate = {
        id: this.client.id,
        birthdate: new Date(this.client.birthdate).toISOString() || null,
        nationalityId: this.client.nationalityId,
        userId: this.person.id,
        telephoneNumber: this.client.telephoneNumber,
        location: {
          latitude: this.latitude,
          longitude: this.longitude,
        },
        firstName: this.client.firstName,
        lastName: this.client.lastName,
        email: this.client.email,
        pictureId: this.client.pictureId,
        picture: this.client.picture
      }
      if (this.pictureChange) {
        this.pictureService.create(this.picture).subscribe(data => {
          clientUpdate.pictureId = data.id;
          clientUpdate.picture = data;
          this.accountService.save(this.person).subscribe(data => {
            this.clientService.update(clientUpdate).subscribe(data => {
              let toast = this.toastCtrl.create(
                {message: 'User updated successfully.', duration: 3000, position: 'top'});
                toast.present();
              this.navCtrl.push(Settings);
            }, (error) => console.error(error));
          }, (error) => console.error(error));
        }, (error) => console.error(error));
      } else {
        this.accountService.save(this.person).subscribe(data => {
          this.clientService.update(clientUpdate).subscribe(data => {
            let toast = this.toastCtrl.create(
              {message: 'User updated successfully.', duration: 3000, position: 'top'});
              toast.present();
            this.navCtrl.push(Settings);
          }, (error) => console.error(error));
        }, (error) => console.error(error));
      }
    }
  }

  getAge(birthdate){
    let currentTime = new Date().getTime();
    return ((currentTime - birthdate)/31556952000).toFixed(0);
  }

  ionViewWillEnter() {
    // Build an empty form for the template to render
    this.page = this.navParams.get('page') || this.page;
    this.pageTitleKey = this.pageTitleKey;

    this.translate.get(this.pageTitleKey).subscribe((res) => {
      this.pageTitle = res;
    })
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType) {

    const options: CameraOptions = {
      quality: 90,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.picture.contentType = 'image/png';
      this.picture.content = imageData;
      this.profileImg = this.pathForImage(imageData);
      this.pictureChange = true;
    }, (err) => {
      // Handle error
      this.presentToast('Error while selecting image.');
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 90,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.picture.contentType = 'image/png';
      this.picture.content = imageData;
      this.profileImg = this.pathForImage(imageData);
      this.pictureChange = true;
    }, (err) => {
      // Handle error
    });
  }

  cropImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit:true,
      targetWidth:300,
      targetHeight:300
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.profileImg = this.pathForImage(imageData);
    }, (err) => {
      // Handle error
    });
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.lastImage = newFileName;
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  pathForImage(img) {
    if (img === null) {
      return '../../assets/imgs/profile.jpg';
    } else {
      return 'data:image/jpeg;base64,' + img;
    }
  }

}
