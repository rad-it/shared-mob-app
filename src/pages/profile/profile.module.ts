import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { ClientService } from '../../providers/entities/client.provider';
import { PictureService } from '../entities/picture/picture.provider';
import { AccountService } from '../../providers/auth/account.service';
import { NationalityService } from '../entities/nationality/nationality.provider';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    TranslateModule.forChild()
  ],
  providers: [
    ClientService,
    PictureService,
    AccountService,
    NationalityService,
    ImagePicker,
    Base64,
    File,
    Transfer,
    Camera,
    FilePath,
  ]
})
export class ProfilePageModule {}
