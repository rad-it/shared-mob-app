import { Component, OnInit  } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController } from 'ionic-angular';
import { LocalStorageService } from 'ngx-webstorage';
import { Tab1Root } from '../pages';
import { MyBookings } from '../pages';
import { Tab3Root } from '../pages';
import { Tab4Root } from '../pages'
import { Events } from 'ionic-angular';
import { Notification } from "../../models/entities/notification.model";
import { NotificationService } from '../../providers/entities/notification.provider';
import { Principal } from '../../providers/auth/principal.service';


@IonicPage({ priority: 'high', segment: 'tabs' })
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit {
  tab1Root: any = Tab1Root;
  tab2Root: any = MyBookings;
  tab3Root: any = Tab3Root;
  tab4Root: any = Tab4Root;
  unreadNotif: any;
  filter: any = {};
  notifications: Notification[];

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";
  tab4Title = " ";

  constructor(public navCtrl: NavController,
    public translateService: TranslateService,
    private localStorage: LocalStorageService,
    public events: Events,
    private notificationService: NotificationService,
    private principal: Principal) {
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE']).subscribe(values => {
      this.tab1Title = values['TAB1_TITLE'];
      this.tab2Title = values['TAB2_TITLE'];
      this.tab3Title = values['TAB3_TITLE'];
      this.tab4Title = values['TAB4_TITLE'];
      this.filter.page = 0;
      this.filter.size = 1;
    });
    
  }  
  ngOnInit() {
     this.events.subscribe('notifications:unread', () => {
       let clientId = this.localStorage.retrieve('clientId');
       if (clientId != null){
       this.notificationService.getCountOfUnreadNotifications(clientId).subscribe((res) => {
        this.unreadNotif = res.body.unread;
         if (this.unreadNotif == 0) {
           this.unreadNotif = "";
         } else {
           if (this.unreadNotif > 10) {
             this.unreadNotif = "+10";
           }
         }
       })
     } else {
       this.unreadNotif = "";
     }

     })

     this.principal.getAuthenticationState().subscribe( (state: any )  =>{
     })


  }


  /**
   *
   * @returns {number}
   */
  getSelectedIndex() {
        let page = location.hash.substring(location.hash.lastIndexOf('/') + 1);
        page = page.charAt(0).toUpperCase() + page.substring(1) + 'Page';
        const tabs = [this.tab1Root, this.tab2Root, this.tab3Root, this.tab4Root];
        const index = tabs.indexOf(page);
        const entitiesTab = tabs.indexOf(this.tab2Root);
        if(location.hash.includes('entities')) {
      return entitiesTab;
    } else if (index > -1) {
      return index;
    }
  }

  /**
   *
   * @returns {boolean}
   */
  isAuthenticated() {
    let token = this.localStorage.retrieve('authenticationToken');
    return !!token;
  }
}
