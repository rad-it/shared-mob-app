import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { TabsPage } from './tabs';
import { NotificationService } from '../../providers/entities/notification.provider';

@NgModule({
  declarations: [
    TabsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsPage),
    TranslateModule.forChild()
  ],
  providers: [
    NotificationService,
  ],
  exports: [
    TabsPage
  ]
})
export class TabsPageModule { }
