/**
 * Created by German on 26/5/2018.
 */
import { Component, ViewChild, NgZone } from '@angular/core';
import {
  NavParams,
  ViewController,
  Platform,
  ToastController
} from 'ionic-angular';
import { MealClassification } from "../../entities/meal-classification";
import { MapsAPILoader } from '@agm/core';
import { FormControl } from "@angular/forms";
import { } from 'googlemaps';

//Services
import { MealClassificationService } from '../../../providers/entities/meal-classification.provider';
import { TranslateService } from '@ngx-translate/core';

@Component({
  template: `
<ion-header>
  <ion-toolbar color="primary">
    <ion-title>
      {{ 'FILTERS_TITLE' | translate }}
    </ion-title>
       <ion-buttons start>
            <button ion-button (click)="dismiss()">
              <span showWhen="ios" ion-text>Cancel</span>
              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
            </button>
          </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content padding>
  <ion-list>
  <!--Preferencias-->
  <ion-item class="bg-color pl-0">
    <ion-label>{{ 'FILTERS_PREFERENCES' | translate }}</ion-label>
    <ion-select [(ngModel)]="filter.mealClassifications" 
        multiple="true" cancelText="{{ 'CANCEL_BUTTON' | translate }}" okText="{{ 'ADD_BUTTON' | translate }}">

      <ion-option *ngFor="let mealClassification of mealClassifications" 
         value="{{mealClassification.id}}">{{mealClassification.name}}</ion-option>
    </ion-select>
  </ion-item>
  <!--Ubicación-->
  <ion-item class="bg-color pl-0">
      <ion-label floating>{{ 'FILTERS_LOCATION' | translate }}</ion-label>
      <ion-input id="txtHome" type="text" [(ngModel)]="filter.address"></ion-input>
  </ion-item>
</ion-list>
</ion-content>
<ion-footer padding>
     <ion-buttons end>
      <button ion-button (click)="clearFilter()" clear>
          {{ 'CLEAR_BUTTON' | translate }}
      </button>
      <button ion-button color="primary" (click)="dismiss(this.filter)" item-end icon-end>
          {{ 'APPLY_BUTTON' | translate }}
      </button>
    </ion-buttons>
</ion-footer>
`
})
export class ModalFilterPage {
  mealClassifications: MealClassification[];
  filter: any = {};
  public searchControl: FormControl;
  @ViewChild("search")
  public searchElementRef;
  getErrorString:string = '';

  constructor(public platform: Platform,
              public params: NavParams,
              public viewCtrl: ViewController,
              private mealClassificationService: MealClassificationService,
              private toastCtrl: ToastController,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private translateService: TranslateService) {

    if (params.get('filter')) this.filter = params.get('filter');
    else this.clearFilter();

    this.mealClassificationService.query().subscribe(
      (response) => {
        this.mealClassifications = response;
      },
      (error) => {
        let toast = this.toastCtrl.create({message: this.getErrorString, duration: 2000, position: 'top'});
        toast.present();
      });

    //create search FormControl
    this.searchControl = new FormControl();

    this.translateService.get('GET_ERROR').subscribe((value) => {
      this.getErrorString = value;
    });
  }

  /**
   *
   * @param data
   */
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

  /**
   * Filters to apply
   */
  clearFilter() {
    this.filter.mealClassifications = [];
    this.filter.ubicacion = '';
    this.filter.lat = '';
    this.filter.lng = '';
    this.filter.address = '';
  }

  /**
   *
   */
  ionViewDidLoad() {
    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let nativeHomeInputBox = document.getElementById('txtHome').getElementsByTagName('input')[0];
      let autocomplete = new google.maps.places.Autocomplete(nativeHomeInputBox, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude
          this.filter.lat = place.geometry.location.lat();
          this.filter.lng = place.geometry.location.lng();
        });
      });
    });
  }

}
