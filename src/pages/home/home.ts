import { Component, OnInit } from '@angular/core';
import {
  App,
  IonicPage,
  NavController,
  ModalController,
  ToastController,
  LoadingController,
  Platform } from 'ionic-angular';
import { Principal } from '../../providers/auth/principal.service';
import { Geolocation } from '@ionic-native/geolocation';
import { tap } from 'rxjs/operators';
import { FcmProvider } from '../../providers/fcm/fcm';

//Services
import { RestaurantService } from '../../providers/entities/restaurant.provider';
import { TranslateService } from '@ngx-translate/core';

//Modals
import { ModalFilterPage  } from "./filter/filter.modal";

//Pages
import { RestaurantDetails } from "../pages";

//Models
import { Restaurant } from '../../models/entities/restaurant.model';
import { Account } from "../../models/account.model";

import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  account: Account;
  restaurants: Restaurant[] = [];
  filter: any = {};
  getErrorString:string = '';
  view: string;
  restaurantSelected: Restaurant;
  loaded: boolean = false;

  constructor(public navCtrl: NavController,
              private principal: Principal,
              private app: App,
              public modalCtrl: ModalController,
              public loadingCrtl: LoadingController,
              private restaurantService: RestaurantService,
              private geolocation: Geolocation,
              private toastCtrl: ToastController,
              private translateService: TranslateService,
              public platform: Platform,
              public fcm: FcmProvider) {
    //Filters
    this.filter.search = '';
    this.filter.page = 0;
    this.filter.size = 10;

    //View
    this.view = 'list';

    this.translateService.get('GET_ERROR').subscribe((value) => {
      this.getErrorString = value;
    });
  }

  ngOnInit() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.filter.lat = resp.coords.latitude;
      this.filter.lng = resp.coords.longitude;
      this.getRestaurants(true);
    }).catch((error) => {
      this.getRestaurants(true);
    });
  }

  ionViewDidLoad(){
  }

  /**
   *
   * @returns {boolean}
   */
  isAuthenticated() {
    return this.principal.isAuthenticated();
  }

  /**
   * Api call to load more products when infinite scroll.
   * @param infiniteScroll
   */
  doInfinite(infiniteScroll: any) {
    setTimeout(() => {
      this.getRestaurants(false, infiniteScroll);
    }, 500);
  }

  /**
   * Api call to refresh products
   * @param refresher
   */
  doRefresh(refresher: any) {
    this.getRestaurants(true, refresher);
  }

  /**
   *
   * @param $event
   * @param refresh
   */
  getRestaurants(refresh: boolean, $event?) {
      if (!refresh) this.filter.page = this.filter.page + 1;
      else this.filter.page = 0;

      // Loader Spinner
      let loader = this.loadingCrtl.create({
        content: '',
        spinner: 'crescent',
      });
      loader.present();

      setTimeout(() => {
        loader.dismiss();
      }, 5000);

      this.restaurantService.query(this.filter)
        .subscribe( res =>  {
          this.loaded = true;
          if (refresh) this.restaurants = res.body;
          else this.restaurants = _.concat(this.restaurants, res.body);
          loader.dismiss();
        },
          (error) => {
            this.loaded = true;
            let toast = this.toastCtrl.create({message: this.getErrorString, duration: 2000, position: 'top'});
            toast.present();
          } );

      if ($event) $event.complete();
  }

  /**
   * Abre modal filtros de búsqueda
   */
  openModalFiltros() {
    let modal = this.modalCtrl.create(ModalFilterPage, {filter: this.filter});
    modal.onDidDismiss(filter => {
      if (filter) {
        this.filter = filter;
        this.getRestaurants(true);
      }
    });
    modal.present();
  }

  /**
   *
   * @param restaurant
   */
  goToRestaurantProfile(restaurant) {
    this.navCtrl.push(RestaurantDetails, {id: restaurant.id});
  }

  /**
   *
   */
  changeView() {
    if(_.isEqual(this.view,'list')) this.view = 'map';
    else this.view = 'list';
  }

  /**
   *
   * @param event
   */
  onSelect(event) {
    this.restaurantSelected = event;
  }

  /**
   *
   * @param event
   */
  onClose(event) {
    this.restaurantSelected = event;
  }
}
