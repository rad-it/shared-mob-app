import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { Geolocation } from '@ionic-native/geolocation';

//Modules
import { ComponentsModule } from '../../components/components.module';

//Services
import { RestaurantService } from '../../providers/entities/restaurant.provider';
import { MealClassificationService } from '../../providers/entities/meal-classification.provider';

@NgModule({
  declarations: [
    HomePage
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    TranslateModule.forChild(),
    ComponentsModule
  ],
  exports: [
    HomePage
  ],
  providers: [
    RestaurantService,
    Geolocation,
    MealClassificationService
  ]
})
export class HomePageModule { }
