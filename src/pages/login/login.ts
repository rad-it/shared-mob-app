import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MainPage, Tab3Root } from '../pages';
import { LoginService } from '../../providers/login/login.service';
import { AccountService } from '../../providers/auth/account.service';
import { ClientService } from '../../providers/entities/client.provider';
import { BookingService } from '../../providers/entities/booking.provider';
import { Booking } from "../../models/entities/booking.model";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  account: { username: string, password: string, rememberMe: boolean } = {
    username: '',
    password: '',
    rememberMe: false,
  };
  public form: FormGroup;
  public booking: Booking;

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
              public loginService: LoginService,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              private accountService: AccountService,
              private clientService: ClientService,
              private bookingService: BookingService,
              private fb: FormBuilder,
              params: NavParams) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    });

    this.form = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      rememberMe: [false],
    });

    if(params.get('booking')) this.booking = params.get('booking');
  }

  /**
   * Attempt to login in through our User service
   */
  doLogin(form) {

    this.loginService.login(form).then((response) => {

      if(this.booking) {

        this.accountService.get().subscribe(
         (accountData) => {
                this.clientService.findByUser(accountData.id).subscribe(
                  (clientData) => {

                    this.booking.petitionerId = clientData.id;
                    this.bookingService.create(this.booking).subscribe(data => {
                      this.translateService.get(['BOOKING', 'REGISTERED']).subscribe(translations => {
                        let toast = this.toastCtrl.create(
                          {message: translations.BOOKING + ' ' + translations.REGISTERED, duration: 3000, position: 'middle'});
                        toast.present();
                      });
                      this.navCtrl.setRoot( MainPage , {});
                    }, (error) => console.error(error));

                  }
                );
            }
         );

      } else this.navCtrl.setRoot( Tab3Root , {});
    }, (err) => {
      // Unable to log in
      this.account.password = '';
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }

  /**
   *
   */
  signup() {
    this.navCtrl.push('SignupPage', {booking: this.booking});
  }
}
