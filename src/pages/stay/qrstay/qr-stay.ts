import { Component, OnInit } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController
} from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ClientTable } from '../../../models/entities/client-table.model';
import { Restaurant } from '../../../models/entities/restaurant.model';
import { Toast } from '@ionic-native/toast';
import { BookingDetail } from '../../pages';
import { ModalController, Platform, ViewController } from 'ionic-angular';
//  import { DataServiceProvider } from '../../providers/data-service/data-service';

//Services
import { TranslateService } from '@ngx-translate/core';

import { LocalStorageService } from 'ngx-webstorage';
import { AlertController } from 'ionic-angular';

import * as _ from 'lodash';
import { ClientTableService } from '../../../providers/entities/client-table.provider';
import { RestaurantService } from '../../../providers/entities/restaurant.provider';
import { StayService } from '../../../providers/entities/stay.provider';
/**
 * Generated class for the MyBookingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var $ :any;

@IonicPage({
  segment: 'qr-stay',
  defaultHistory: ['EntityPage', 'QrStayPage'],

})

@Component({
  selector: 'page-my-bookings',
  templateUrl: 'qr-stay.html',
  styleUrls: ['/pages/stay/qrstay/qr-stay.scss'],
})
export class QrStayPage implements OnInit {

  products: any[] = [];
  selectedProduct: any;

  title: string;
  confirm: string;
  cancel: string;
  message: string;
  people: string;
  clientTableStay: ClientTable;
  restaurantStay: Restaurant;

  filter: any = {};
  getErrorString: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private localStorage: LocalStorageService,
    public loadingCrtl: LoadingController,
    private translateService: TranslateService,
    private clientTableService: ClientTableService,
    private restaurantService: RestaurantService,
    private barcodeScanner: BarcodeScanner,
    private stayService:StayService,
    private toast: Toast,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,

  ) {

    this.filter.page = 0;
    this.filter.size = 10;

    this.translateService.get('GET_ERROR').subscribe((value) => {
      this.getErrorString = value;
    });
  }

  ngOnInit() {
    let options = {
      prompt: "Coloque un codigo QR en el visor del rectángulo"
    }

    this.barcodeScanner.scan(options).then(barcodeData => {
      this.openModal(barcodeData);
      this.navCtrl.remove(1);
     }).catch(err => {
         console.log('Error', err);
     });
}

//Inicio componente modal
openModal(barcodeData) {

  let modal = this.modalCtrl.create('ModalPage', {data: barcodeData} );
  modal.present();
}



  /**
   *
   */
  ionViewWillEnter() {
  }

  ionViewDidLeave() {

  }  

}

