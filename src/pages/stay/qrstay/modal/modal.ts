import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, } from 'ionic-angular';
import { ClientTableService } from '../../../../providers/entities/client-table.provider';
import { RestaurantService } from '../../../../providers/entities/restaurant.provider';
import { StayService } from '../../../../providers/entities/stay.provider';
import { ClientTable } from '../../../../models/entities/client-table.model';
import { Restaurant } from '../../../../models/entities/restaurant.model';
import { BookingDetail } from '../../../pages';
import { LocalStorageService } from 'ngx-webstorage';
import { MyBookings } from '../../../pages';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage implements OnInit {

  barcode: any;
  clientTableStay = new ClientTable;
  restaurant = new Restaurant;
  getErrorString:string = '';
  people: number = 0;
  isLoading: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private stayService: StayService,
    private toastCtrl: ToastController,
    private restaurantService: RestaurantService,
    private clientTableService: ClientTableService,
    private translateService: TranslateService,
    private localStorage: LocalStorageService,
    public viewCtrl: ViewController) {
      this.translateService.get('STAY_ERROR').subscribe((value) => {
        this.getErrorString = value;
      });
  }

  ngOnInit() {
    this.barcode = this.navParams.get('data');

    let codeQr = (this.barcode.text).replace("'", '"');
    codeQr = JSON.parse(codeQr);
    console.log(codeQr.tableId);
    this.clientTableService.find(codeQr.tableId).subscribe((clientTable: ClientTable) => {
      this.clientTableStay = clientTable;
      this.people = this.clientTableStay.chairs != null ? this.clientTableStay.chairs : 1;
      console.log(this.clientTableStay);
      this.restaurantService.find(codeQr.restaurantId).subscribe((restaurant: Restaurant) => {
        this.restaurant = restaurant;
        console.log(this.restaurant);
      })
    })
  }

  confirm() {
    let clientId = this.localStorage.retrieve('clientId');
    this.isLoading  = true;
    let body = { 'restaurantId': this.restaurant.id, 'tableId': this.clientTableStay.id, 'people': this.people }
    this.stayService.createByClientId(clientId, body).subscribe(res => {
      //Redirect to Stay Profile
          this.navCtrl.push(BookingDetail, { restaurantId: this.restaurant.id, stayId: res.id } )
          this.isLoading = false;
          this.dismiss();
    },(error) => {
      let toast = this.toastCtrl.create({ message: this.getErrorString, duration: 2000, position: 'top' });
      this.isLoading = false;
      toast.present();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss()
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  increment() {
    this.people++
  }

  decrement() {
    this.people--;
  }

  ionViewWillLoad() {


  }
}
