import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalPage } from './modal';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../../components/components.module';

//Services
import { ClientTableService } from '../../../../providers/entities/client-table.provider';
import { RestaurantService } from '../../../../providers/entities/restaurant.provider';
import { StayService } from '../../../../providers/entities/stay.provider';

@NgModule({
  declarations: [
    ModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalPage),
    TranslateModule.forChild(),
    ComponentsModule,
  ],
  providers: [
    RestaurantService,
    ClientTableService,
    StayService
  ]
})
export class ModalPageModule {}
