import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { QrStayPage } from './qr-stay';
 import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Toast } from '@ionic-native/toast';

//Services
import { ClientTableService } from '../../../providers/entities/client-table.provider';
import { RestaurantService } from '../../../providers/entities/restaurant.provider';
import { StayService } from '../../../providers/entities/stay.provider';


@NgModule({
  declarations: [
     QrStayPage,
    
  ],
 
  imports: [
   
    IonicPageModule.forChild(QrStayPage),
    // IonicPageModule.forChild(ModalContentPage),
    TranslateModule.forChild()
  ],
  exports: [
    QrStayPage,
],
  providers: [
    RestaurantService,
    ClientTableService,
    BarcodeScanner,
    Toast,
    StayService
  ]
})
export class QrStayPageModule {}
