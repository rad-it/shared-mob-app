import { BaseEntity } from './../../../models';
import { RestaurantType } from '../restaurant-type';
import { Picture } from '../picture';

export class Restaurant implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public cuit?: string,
        public cbu?: string,
        public locationId?: number,
        public location?: Location,
        public pictures?: Picture[],
        public logo?: any,
        public roles?: BaseEntity[],
        public sectors?: BaseEntity[],
        public restaurantTypeId?: number,
        public restaurantType?: RestaurantType,
        public mealClassifications?: BaseEntity[],
    ) {
    }
}
