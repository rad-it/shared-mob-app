import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Restaurant } from './restaurant.model';
import { RestaurantService } from './restaurant.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-restaurant',
    templateUrl: 'restaurant.html'
})
export class RestaurantPage {
    restaurants: Restaurant[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private restaurantService: RestaurantService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.restaurants = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.restaurantService.query().subscribe(
            (response) => {
                this.restaurants = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Restaurant) {
        return item.id;
    }

    open(slidingItem: any, item: Restaurant) {
        let modal = this.modalCtrl.create('RestaurantDialogPage', {item: item});
        modal.onDidDismiss(restaurant => {
            if (restaurant) {
                if (restaurant.id) {
                    this.restaurantService.update(restaurant).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Restaurant updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.restaurantService.create(restaurant).subscribe(data => {
                        this.restaurants.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Restaurant added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(restaurant) {
        this.restaurantService.delete(restaurant.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Restaurant deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(restaurant: Restaurant) {
        this.navCtrl.push('RestaurantDetailPage', {id: restaurant.id});
    }
}
