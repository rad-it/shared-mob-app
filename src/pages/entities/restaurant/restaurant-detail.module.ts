import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantDetailPage } from './restaurant-detail';
import { RestaurantService } from './restaurant.provider';

@NgModule({
    declarations: [
        RestaurantDetailPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantDetailPage
    ],
    providers: [RestaurantService]
})
export class RestaurantDetailPageModule {
}
