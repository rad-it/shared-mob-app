import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Restaurant } from './restaurant.model';
import { RestaurantService } from './restaurant.provider';
import { Location, LocationService } from '../location';
import { RestaurantType, RestaurantTypeService } from '../restaurant-type';
import { MealClassification, MealClassificationService } from '../meal-classification';

@IonicPage()
@Component({
    selector: 'page-restaurant-dialog',
    templateUrl: 'restaurant-dialog.html'
})
export class RestaurantDialogPage {

    restaurant: Restaurant;
    locations: Location[];
    restauranttypes: RestaurantType[];
    mealclassifications: MealClassification[];
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private locationService: LocationService,
                private restaurantTypeService: RestaurantTypeService,
                private mealClassificationService: MealClassificationService,
                private restaurantService: RestaurantService) {
        this.restaurant = params.get('item');
        if (this.restaurant && this.restaurant.id) {
            this.restaurantService.find(this.restaurant.id).subscribe(data => {
                this.restaurant = data;
            });
        } else {
            this.restaurant = new Restaurant();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.restaurant.id : null],
            name: [params.get('item') ? this.restaurant.name : '',  Validators.required],
            description: [params.get('item') ? this.restaurant.description : '', ],
            cuit: [params.get('item') ? this.restaurant.cuit : '',  Validators.required],
            cbu: [params.get('item') ? this.restaurant.cbu : '', ],
            location: [params.get('item') ? this.restaurant.location : '',],
            restaurantType: [params.get('item') ? this.restaurant.restaurantType : '',],
            mealClassifications: [params.get('item') ? this.restaurant.mealClassifications : '',],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
        this.locationService
            .query({filter: 'restaurant-is-null'})
            .subscribe(data => {
                if (!this.restaurant.locationId) {
                    this.locations = data;
                } else {
                    this.locationService
                        .find(this.restaurant.locationId)
                        .subscribe((subData: Location) => {
                            this.locations = [subData].concat(subData);
                        }, (error) => this.onError(error));
                }
            }, (error) => this.onError(error));
        this.restaurantTypeService.query()
            .subscribe(data => { this.restauranttypes = data; }, (error) => this.onError(error));
        this.mealClassificationService.query()
            .subscribe(data => { this.mealclassifications = data; }, (error) => this.onError(error));
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the restaurant, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    compareLocation(first: Location, second: Location): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackLocationById(index: number, item: Location) {
        return item.id;
    }
    compareRestaurantType(first: RestaurantType, second: RestaurantType): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackRestaurantTypeById(index: number, item: RestaurantType) {
        return item.id;
    }
    compareMealClassification(first: MealClassification, second: MealClassification): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMealClassificationById(index: number, item: MealClassification) {
        return item.id;
    }
}
