export * from './restaurant.model';
export * from './restaurant.provider';
export * from './restaurant-dialog';
export * from './restaurant-detail';
export * from './restaurant';
