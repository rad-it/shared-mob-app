import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { Restaurant } from './restaurant.model';
import { RestaurantService } from './restaurant.provider';

@IonicPage({
    segment: 'restaurant-detail/:id',
    defaultHistory: ['EntityPage', 'restaurantPage']
})
@Component({
    selector: 'page-restaurant-detail',
    templateUrl: 'restaurant-detail.html'
})
export class RestaurantDetailPage {
    restaurant: Restaurant;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private restaurantService: RestaurantService, private toastCtrl: ToastController) {
        this.restaurant = new Restaurant();
        this.restaurant.id = params.get('id');
    }

    ionViewDidLoad() {
        this.restaurantService.find(this.restaurant.id).subscribe(data => this.restaurant = data);
    }

    open(item: Restaurant) {
        let modal = this.modalCtrl.create('RestaurantDialogPage', {item: item});
        modal.onDidDismiss(restaurant => {
            if (restaurant) {
                this.restaurantService.update(restaurant).subscribe(data => {
                    this.restaurant = data;
                    let toast = this.toastCtrl.create(
                        {message: 'Restaurant updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
