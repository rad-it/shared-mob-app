import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantPage } from './restaurant';
import { RestaurantService } from './restaurant.provider';

@NgModule({
    declarations: [
        RestaurantPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantPage),
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantPage
    ],
    providers: [RestaurantService]
})
export class RestaurantPageModule {
}
