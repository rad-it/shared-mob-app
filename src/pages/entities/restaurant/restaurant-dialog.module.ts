import { LocationService } from '../location';
import { RestaurantTypeService } from '../restaurant-type';
import { MealClassificationService } from '../meal-classification';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantDialogPage } from './restaurant-dialog';
import { RestaurantService } from './restaurant.provider';

@NgModule({
    declarations: [
        RestaurantDialogPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantDialogPage
    ],
    providers: [
        RestaurantService,
        LocationService,
        RestaurantTypeService,
        MealClassificationService,
    ]
})
export class RestaurantDialogPageModule {
}
