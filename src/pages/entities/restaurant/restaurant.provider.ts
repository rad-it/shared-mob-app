import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { Restaurant } from './restaurant.model';

@Injectable()
export class RestaurantService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/restaurants';

    constructor(private http: HttpClient) { }

    create(restaurant: Restaurant): Observable<Restaurant> {
        return this.http.post(this.resourceUrl, restaurant);
    }

    update(restaurant: Restaurant): Observable<Restaurant> {
        return this.http.put(this.resourceUrl, restaurant);
    }

    find(id: number): Observable<Restaurant> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
