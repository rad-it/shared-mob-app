import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuPage } from './menu';
import { MenuService } from './menu.provider';

@NgModule({
    declarations: [
        MenuPage
    ],
    imports: [
        IonicPageModule.forChild(MenuPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuPage
    ],
    providers: [MenuService]
})
export class MenuPageModule {
}
