import { MeasuringUnitService } from '../measuring-unit';
import { MenuClassificationService } from '../menu-classification';
import { RestaurantService } from '../restaurant';
import { PictureService } from '../picture';
import { MenuSubclassificationService } from '../menu-subclassification';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuDialogPage } from './menu-dialog';
import { MenuService } from './menu.provider';

@NgModule({
    declarations: [
        MenuDialogPage
    ],
    imports: [
        IonicPageModule.forChild(MenuDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuDialogPage
    ],
    providers: [
        MenuService,
        MeasuringUnitService,
        MenuClassificationService,
        RestaurantService,
        PictureService,
        MenuSubclassificationService,
    ]
})
export class MenuDialogPageModule {
}
