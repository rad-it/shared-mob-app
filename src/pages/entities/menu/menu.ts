import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Menu } from './menu.model';
import { MenuService } from './menu.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html'
})
export class MenuPage {
    menus: Menu[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private menuService: MenuService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.menus = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.menuService.query().subscribe(
            (response) => {
                this.menus = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Menu) {
        return item.id;
    }

    open(slidingItem: any, item: Menu) {
        let modal = this.modalCtrl.create('MenuDialogPage', {item: item});
        modal.onDidDismiss(menu => {
            if (menu) {
                if (menu.id) {
                    this.menuService.update(menu).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Menu updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.menuService.create(menu).subscribe(data => {
                        this.menus.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Menu added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(menu) {
        this.menuService.delete(menu.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Menu deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(menu: Menu) {

    }
}
