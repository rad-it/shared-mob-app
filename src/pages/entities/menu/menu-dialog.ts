import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Menu } from './menu.model';
import { MenuService } from './menu.provider';
import { MeasuringUnit, MeasuringUnitService } from '../measuring-unit';
import { MenuClassification, MenuClassificationService } from '../menu-classification';
import { Restaurant, RestaurantService } from '../restaurant';
import { Picture, PictureService } from '../picture';
import { MenuSubclassification, MenuSubclassificationService } from '../menu-subclassification';

@IonicPage()
@Component({
    selector: 'page-menu-dialog',
    templateUrl: 'menu-dialog.html'
})
export class MenuDialogPage {

    menu: Menu;
    measuringunits: MeasuringUnit[];
    menuclassifications: MenuClassification[];
    restaurants: Restaurant[];
    pictures: Picture[];
    menusubclassifications: MenuSubclassification[];
    deleteDate: string;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private measuringUnitService: MeasuringUnitService,
                private menuClassificationService: MenuClassificationService,
                private restaurantService: RestaurantService,
                private pictureService: PictureService,
                private menuSubclassificationService: MenuSubclassificationService,
                private menuService: MenuService) {
        this.menu = params.get('item');
        if (this.menu && this.menu.id) {
            this.menuService.find(this.menu.id).subscribe(data => {
                this.menu = data;
            });
        } else {
            this.menu = new Menu();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.menu.id : null],
            name: [params.get('item') ? this.menu.name : '',  Validators.required],
            description: [params.get('item') ? this.menu.description : '', ],
            price: [params.get('item') ? this.menu.price : '',  Validators.required],
            quantity: [params.get('item') ? this.menu.quantity : '',  Validators.required],
            destination: [params.get('item') ? this.menu.destination : '', ],
            disabled: [params.get('item') ? this.menu.disabled : 'false', ],
            deleteDate: [params.get('item') ? this.menu.deleteDate : '', ],
            measuringUnit: [params.get('item') ? this.menu.measuringUnit : '',],
            menuClassification: [params.get('item') ? this.menu.classification : '',],
            restaurant: [params.get('item') ? this.menu.restaurant : '',],
            pictures: [params.get('item') ? this.menu.pictures : '',],
            menuSubclassifications: [params.get('item') ? this.menu.subclassifications : '',],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
        this.measuringUnitService.query()
            .subscribe(data => { this.measuringunits = data; }, (error) => this.onError(error));
        this.menuClassificationService.query()
            .subscribe(data => { this.menuclassifications = data; }, (error) => this.onError(error));
        this.restaurantService.query()
            .subscribe(data => { this.restaurants = data; }, (error) => this.onError(error));
        this.pictureService.query()
            .subscribe(data => { this.pictures = data; }, (error) => this.onError(error));
        this.menuSubclassificationService.query()
            .subscribe(data => { this.menusubclassifications = data; }, (error) => this.onError(error));
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the menu, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    compareMeasuringUnit(first: MeasuringUnit, second: MeasuringUnit): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMeasuringUnitById(index: number, item: MeasuringUnit) {
        return item.id;
    }
    compareMenuClassification(first: MenuClassification, second: MenuClassification): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMenuClassificationById(index: number, item: MenuClassification) {
        return item.id;
    }
    compareRestaurant(first: Restaurant, second: Restaurant): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }
    comparePicture(first: Picture, second: Picture): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackPictureById(index: number, item: Picture) {
        return item.id;
    }
    compareMenuSubclassification(first: MenuSubclassification, second: MenuSubclassification): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMenuSubclassificationById(index: number, item: MenuSubclassification) {
        return item.id;
    }
}
