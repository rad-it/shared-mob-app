import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuService } from './menu.provider';

@NgModule({
    declarations: [

    ],
    imports: [
      IonicPageModule,
      TranslateModule.forChild()
    ],
    exports: [
    ],
    providers: [MenuService]
})
export class MenuDetailPageModule {
}
