import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantTypeDetailPage } from './restaurant-type-detail';
import { RestaurantTypeService } from './restaurant-type.provider';

@NgModule({
    declarations: [
        RestaurantTypeDetailPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantTypeDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantTypeDetailPage
    ],
    providers: [RestaurantTypeService]
})
export class RestaurantTypeDetailPageModule {
}
