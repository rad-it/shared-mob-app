import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { RestaurantType } from './restaurant-type.model';
import { RestaurantTypeService } from './restaurant-type.provider';

@IonicPage()
@Component({
    selector: 'page-restaurant-type-dialog',
    templateUrl: 'restaurant-type-dialog.html'
})
export class RestaurantTypeDialogPage {

    restaurantType: RestaurantType;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private restaurantTypeService: RestaurantTypeService) {
        this.restaurantType = params.get('item');
        if (this.restaurantType && this.restaurantType.id) {
            this.restaurantTypeService.find(this.restaurantType.id).subscribe(data => {
                this.restaurantType = data;
            });
        } else {
            this.restaurantType = new RestaurantType();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.restaurantType.id : null],
            name: [params.get('item') ? this.restaurantType.name : '',  Validators.required],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the restaurant-type, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
