import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { RestaurantType } from './restaurant-type.model';

@Injectable()
export class RestaurantTypeService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/restaurant-types';

    constructor(private http: HttpClient) { }

    create(restaurantType: RestaurantType): Observable<RestaurantType> {
        return this.http.post(this.resourceUrl, restaurantType);
    }

    update(restaurantType: RestaurantType): Observable<RestaurantType> {
        return this.http.put(this.resourceUrl, restaurantType);
    }

    find(id: number): Observable<RestaurantType> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
