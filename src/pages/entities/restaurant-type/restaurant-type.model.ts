import { BaseEntity } from './../../../models';

export class RestaurantType implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
