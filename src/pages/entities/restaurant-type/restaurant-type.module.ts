import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantTypePage } from './restaurant-type';
import { RestaurantTypeService } from './restaurant-type.provider';

@NgModule({
    declarations: [
        RestaurantTypePage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantTypePage),
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantTypePage
    ],
    providers: [RestaurantTypeService]
})
export class RestaurantTypePageModule {
}
