import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { RestaurantType } from './restaurant-type.model';
import { RestaurantTypeService } from './restaurant-type.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-restaurant-type',
    templateUrl: 'restaurant-type.html'
})
export class RestaurantTypePage {
    restaurantTypes: RestaurantType[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private restaurantTypeService: RestaurantTypeService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.restaurantTypes = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.restaurantTypeService.query().subscribe(
            (response) => {
                this.restaurantTypes = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: RestaurantType) {
        return item.id;
    }

    open(slidingItem: any, item: RestaurantType) {
        let modal = this.modalCtrl.create('RestaurantTypeDialogPage', {item: item});
        modal.onDidDismiss(restaurantType => {
            if (restaurantType) {
                if (restaurantType.id) {
                    this.restaurantTypeService.update(restaurantType).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'RestaurantType updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.restaurantTypeService.create(restaurantType).subscribe(data => {
                        this.restaurantTypes.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'RestaurantType added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(restaurantType) {
        this.restaurantTypeService.delete(restaurantType.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'RestaurantType deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(restaurantType: RestaurantType) {
        this.navCtrl.push('RestaurantTypeDetailPage', {id: restaurantType.id});
    }
}
