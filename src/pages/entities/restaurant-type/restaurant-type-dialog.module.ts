import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantTypeDialogPage } from './restaurant-type-dialog';
import { RestaurantTypeService } from './restaurant-type.provider';

@NgModule({
    declarations: [
        RestaurantTypeDialogPage
    ],
    imports: [
        IonicPageModule.forChild(RestaurantTypeDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        RestaurantTypeDialogPage
    ],
    providers: [
        RestaurantTypeService
    ]
})
export class RestaurantTypeDialogPageModule {
}
