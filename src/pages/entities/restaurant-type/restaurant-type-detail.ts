import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { RestaurantType } from './restaurant-type.model';
import { RestaurantTypeService } from './restaurant-type.provider';

@IonicPage({
    segment: 'restaurant-type-detail/:id',
    defaultHistory: ['EntityPage', 'restaurant-typePage']
})
@Component({
    selector: 'page-restaurant-type-detail',
    templateUrl: 'restaurant-type-detail.html'
})
export class RestaurantTypeDetailPage {
    restaurantType: RestaurantType;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private restaurantTypeService: RestaurantTypeService, private toastCtrl: ToastController) {
        this.restaurantType = new RestaurantType();
        this.restaurantType.id = params.get('id');
    }

    ionViewDidLoad() {
        this.restaurantTypeService.find(this.restaurantType.id).subscribe(data => this.restaurantType = data);
    }

    open(item: RestaurantType) {
        let modal = this.modalCtrl.create('RestaurantTypeDialogPage', {item: item});
        modal.onDidDismiss(restaurantType => {
            if (restaurantType) {
                this.restaurantTypeService.update(restaurantType).subscribe(data => {
                    this.restaurantType = data;
                    let toast = this.toastCtrl.create(
                        {message: 'RestaurantType updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
