import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MeasuringUnitPage } from './measuring-unit';
import { MeasuringUnitService } from './measuring-unit.provider';

@NgModule({
    declarations: [
        MeasuringUnitPage
    ],
    imports: [
        IonicPageModule.forChild(MeasuringUnitPage),
        TranslateModule.forChild()
    ],
    exports: [
        MeasuringUnitPage
    ],
    providers: [MeasuringUnitService]
})
export class MeasuringUnitPageModule {
}
