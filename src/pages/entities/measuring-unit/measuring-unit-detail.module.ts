import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MeasuringUnitDetailPage } from './measuring-unit-detail';
import { MeasuringUnitService } from './measuring-unit.provider';

@NgModule({
    declarations: [
        MeasuringUnitDetailPage
    ],
    imports: [
        IonicPageModule.forChild(MeasuringUnitDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        MeasuringUnitDetailPage
    ],
    providers: [MeasuringUnitService]
})
export class MeasuringUnitDetailPageModule {
}
