import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { MeasuringUnit } from './measuring-unit.model';
import { MeasuringUnitService } from './measuring-unit.provider';

@IonicPage()
@Component({
    selector: 'page-measuring-unit-dialog',
    templateUrl: 'measuring-unit-dialog.html'
})
export class MeasuringUnitDialogPage {

    measuringUnit: MeasuringUnit;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private measuringUnitService: MeasuringUnitService) {
        this.measuringUnit = params.get('item');
        if (this.measuringUnit && this.measuringUnit.id) {
            this.measuringUnitService.find(this.measuringUnit.id).subscribe(data => {
                this.measuringUnit = data;
            });
        } else {
            this.measuringUnit = new MeasuringUnit();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.measuringUnit.id : null],
            name: [params.get('item') ? this.measuringUnit.name : '',  Validators.required],
            code: [params.get('item') ? this.measuringUnit.code : '',  Validators.required],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the measuring-unit, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
