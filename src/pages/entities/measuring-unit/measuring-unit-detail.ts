import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { MeasuringUnit } from './measuring-unit.model';
import { MeasuringUnitService } from './measuring-unit.provider';

@IonicPage({
    segment: 'measuring-unit-detail/:id',
    defaultHistory: ['EntityPage', 'measuring-unitPage']
})
@Component({
    selector: 'page-measuring-unit-detail',
    templateUrl: 'measuring-unit-detail.html'
})
export class MeasuringUnitDetailPage {
    measuringUnit: MeasuringUnit;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private measuringUnitService: MeasuringUnitService, private toastCtrl: ToastController) {
        this.measuringUnit = new MeasuringUnit();
        this.measuringUnit.id = params.get('id');
    }

    ionViewDidLoad() {
        this.measuringUnitService.find(this.measuringUnit.id).subscribe(data => this.measuringUnit = data);
    }

    open(item: MeasuringUnit) {
        let modal = this.modalCtrl.create('MeasuringUnitDialogPage', {item: item});
        modal.onDidDismiss(measuringUnit => {
            if (measuringUnit) {
                this.measuringUnitService.update(measuringUnit).subscribe(data => {
                    this.measuringUnit = data;
                    let toast = this.toastCtrl.create(
                        {message: 'MeasuringUnit updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
