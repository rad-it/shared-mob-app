import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { MeasuringUnit } from './measuring-unit.model';

@Injectable()
export class MeasuringUnitService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/measuring-units';

    constructor(private http: HttpClient) { }

    create(measuringUnit: MeasuringUnit): Observable<MeasuringUnit> {
        return this.http.post(this.resourceUrl, measuringUnit);
    }

    update(measuringUnit: MeasuringUnit): Observable<MeasuringUnit> {
        return this.http.put(this.resourceUrl, measuringUnit);
    }

    find(id: number): Observable<MeasuringUnit> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
