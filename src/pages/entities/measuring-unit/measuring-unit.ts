import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { MeasuringUnit } from './measuring-unit.model';
import { MeasuringUnitService } from './measuring-unit.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-measuring-unit',
    templateUrl: 'measuring-unit.html'
})
export class MeasuringUnitPage {
    measuringUnits: MeasuringUnit[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private measuringUnitService: MeasuringUnitService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.measuringUnits = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.measuringUnitService.query().subscribe(
            (response) => {
                this.measuringUnits = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: MeasuringUnit) {
        return item.id;
    }

    open(slidingItem: any, item: MeasuringUnit) {
        let modal = this.modalCtrl.create('MeasuringUnitDialogPage', {item: item});
        modal.onDidDismiss(measuringUnit => {
            if (measuringUnit) {
                if (measuringUnit.id) {
                    this.measuringUnitService.update(measuringUnit).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'MeasuringUnit updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.measuringUnitService.create(measuringUnit).subscribe(data => {
                        this.measuringUnits.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'MeasuringUnit added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(measuringUnit) {
        this.measuringUnitService.delete(measuringUnit.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'MeasuringUnit deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(measuringUnit: MeasuringUnit) {
        this.navCtrl.push('MeasuringUnitDetailPage', {id: measuringUnit.id});
    }
}
