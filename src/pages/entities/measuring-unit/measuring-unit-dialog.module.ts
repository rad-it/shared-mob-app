import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MeasuringUnitDialogPage } from './measuring-unit-dialog';
import { MeasuringUnitService } from './measuring-unit.provider';

@NgModule({
    declarations: [
        MeasuringUnitDialogPage
    ],
    imports: [
        IonicPageModule.forChild(MeasuringUnitDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        MeasuringUnitDialogPage
    ],
    providers: [
        MeasuringUnitService
    ]
})
export class MeasuringUnitDialogPageModule {
}
