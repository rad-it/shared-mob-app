import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PictureDetailPage } from './picture-detail';
import { PictureService } from './picture.provider';

@NgModule({
    declarations: [
        PictureDetailPage
    ],
    imports: [
        IonicPageModule.forChild(PictureDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        PictureDetailPage
    ],
    providers: [PictureService]
})
export class PictureDetailPageModule {
}
