import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PicturePage } from './picture';
import { PictureService } from './picture.provider';

@NgModule({
    declarations: [
        PicturePage
    ],
    imports: [
        IonicPageModule.forChild(PicturePage),
        TranslateModule.forChild()
    ],
    exports: [
        PicturePage
    ],
    providers: [PictureService]
})
export class PicturePageModule {
}
