import { RestaurantService } from '../restaurant';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PictureDialogPage } from './picture-dialog';
import { PictureService } from './picture.provider';

@NgModule({
    declarations: [
        PictureDialogPage
    ],
    imports: [
        IonicPageModule.forChild(PictureDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        PictureDialogPage
    ],
    providers: [
        PictureService,
        RestaurantService,
    ]
})
export class PictureDialogPageModule {
}
