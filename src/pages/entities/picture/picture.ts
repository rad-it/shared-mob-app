import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { JhiDataUtils } from 'ng-jhipster';
import { Picture } from '../../../models/entities/picture.model';
import { PictureService } from './picture.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-picture',
    templateUrl: 'picture.html'
})
export class PicturePage {
    pictures: Picture[];

    // todo: add pagination

    constructor(private dataUtils: JhiDataUtils,private navCtrl: NavController, private pictureService: PictureService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.pictures = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.pictureService.query().subscribe(
            (response) => {
                this.pictures = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Picture) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    open(slidingItem: any, item: Picture) {
        let modal = this.modalCtrl.create('PictureDialogPage', {item: item});
        modal.onDidDismiss(picture => {
            if (picture) {
                if (picture.id) {
                    this.pictureService.update(picture).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Picture updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.pictureService.create(picture).subscribe(data => {
                        this.pictures.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Picture added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(picture) {
        this.pictureService.delete(picture.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Picture deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(picture: Picture) {
        this.navCtrl.push('PictureDetailPage', {id: picture.id});
    }
}
