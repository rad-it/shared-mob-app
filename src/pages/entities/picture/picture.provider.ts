import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { Picture } from '../../../models/entities/picture.model';

@Injectable()
export class PictureService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/pictures';

    constructor(private http: HttpClient) { }

    create(picture: Picture): Observable<Picture> {
        return this.http.post(this.resourceUrl, picture);
    }

    update(picture: Picture): Observable<Picture> {
        return this.http.put(this.resourceUrl, picture);
    }

    find(id: number): Observable<Picture> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
