import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { JhiDataUtils } from 'ng-jhipster';
import { Picture } from '../../../models/entities/picture.model';
import { PictureService } from './picture.provider';

@IonicPage({
    segment: 'picture-detail/:id',
    defaultHistory: ['EntityPage', 'picturePage']
})
@Component({
    selector: 'page-picture-detail',
    templateUrl: 'picture-detail.html'
})
export class PictureDetailPage {
    picture: Picture;

    constructor(private dataUtils: JhiDataUtils, private modalCtrl: ModalController, params: NavParams,
                private pictureService: PictureService, private toastCtrl: ToastController) {
        this.picture = new Picture();
        this.picture.id = params.get('id');
    }

    ionViewDidLoad() {
        this.pictureService.find(this.picture.id).subscribe(data => this.picture = data);
    }

    open(item: Picture) {
        let modal = this.modalCtrl.create('PictureDialogPage', {item: item});
        modal.onDidDismiss(picture => {
            if (picture) {
                this.pictureService.update(picture).subscribe(data => {
                    this.picture = data;
                    let toast = this.toastCtrl.create(
                        {message: 'Picture updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
}
