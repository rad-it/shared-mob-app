import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MealClassificationDetailPage } from './meal-classification-detail';
import { MealClassificationService } from './meal-classification.provider';

@NgModule({
    declarations: [
        MealClassificationDetailPage
    ],
    imports: [
        IonicPageModule.forChild(MealClassificationDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        MealClassificationDetailPage
    ],
    providers: [MealClassificationService]
})
export class MealClassificationDetailPageModule {
}
