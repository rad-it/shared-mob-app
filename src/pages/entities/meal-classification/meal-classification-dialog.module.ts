import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MealClassificationDialogPage } from './meal-classification-dialog';
import { MealClassificationService } from './meal-classification.provider';

@NgModule({
    declarations: [
        MealClassificationDialogPage
    ],
    imports: [
        IonicPageModule.forChild(MealClassificationDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        MealClassificationDialogPage
    ],
    providers: [
        MealClassificationService
    ]
})
export class MealClassificationDialogPageModule {
}
