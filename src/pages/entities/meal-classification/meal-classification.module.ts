import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MealClassificationPage } from './meal-classification';
import { MealClassificationService } from './meal-classification.provider';

@NgModule({
    declarations: [
        MealClassificationPage
    ],
    imports: [
        IonicPageModule.forChild(MealClassificationPage),
        TranslateModule.forChild()
    ],
    exports: [
        MealClassificationPage
    ],
    providers: [MealClassificationService]
})
export class MealClassificationPageModule {
}
