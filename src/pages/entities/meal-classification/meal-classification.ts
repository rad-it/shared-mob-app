import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { MealClassification } from './meal-classification.model';
import { MealClassificationService } from './meal-classification.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-meal-classification',
    templateUrl: 'meal-classification.html'
})
export class MealClassificationPage {
    mealClassifications: MealClassification[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private mealClassificationService: MealClassificationService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.mealClassifications = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.mealClassificationService.query().subscribe(
            (response) => {
                this.mealClassifications = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: MealClassification) {
        return item.id;
    }

    open(slidingItem: any, item: MealClassification) {
        let modal = this.modalCtrl.create('MealClassificationDialogPage', {item: item});
        modal.onDidDismiss(mealClassification => {
            if (mealClassification) {
                if (mealClassification.id) {
                    this.mealClassificationService.update(mealClassification).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'MealClassification updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.mealClassificationService.create(mealClassification).subscribe(data => {
                        this.mealClassifications.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'MealClassification added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(mealClassification) {
        this.mealClassificationService.delete(mealClassification.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'MealClassification deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(mealClassification: MealClassification) {
        this.navCtrl.push('MealClassificationDetailPage', {id: mealClassification.id});
    }
}
