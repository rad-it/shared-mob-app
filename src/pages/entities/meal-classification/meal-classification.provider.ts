import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { MealClassification } from './meal-classification.model';

@Injectable()
export class MealClassificationService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/meal-classifications';

    constructor(private http: HttpClient) { }

    create(mealClassification: MealClassification): Observable<MealClassification> {
        return this.http.post(this.resourceUrl, mealClassification);
    }

    update(mealClassification: MealClassification): Observable<MealClassification> {
        return this.http.put(this.resourceUrl, mealClassification);
    }

    find(id: number): Observable<MealClassification> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
