import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { MealClassification } from './meal-classification.model';
import { MealClassificationService } from './meal-classification.provider';

@IonicPage()
@Component({
    selector: 'page-meal-classification-dialog',
    templateUrl: 'meal-classification-dialog.html'
})
export class MealClassificationDialogPage {

    mealClassification: MealClassification;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private mealClassificationService: MealClassificationService) {
        this.mealClassification = params.get('item');
        if (this.mealClassification && this.mealClassification.id) {
            this.mealClassificationService.find(this.mealClassification.id).subscribe(data => {
                this.mealClassification = data;
            });
        } else {
            this.mealClassification = new MealClassification();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.mealClassification.id : null],
            name: [params.get('item') ? this.mealClassification.name : '',  Validators.required],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the meal-classification, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
