import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { MealClassification } from './meal-classification.model';
import { MealClassificationService } from './meal-classification.provider';

@IonicPage({
    segment: 'meal-classification-detail/:id',
    defaultHistory: ['EntityPage', 'meal-classificationPage']
})
@Component({
    selector: 'page-meal-classification-detail',
    templateUrl: 'meal-classification-detail.html'
})
export class MealClassificationDetailPage {
    mealClassification: MealClassification;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private mealClassificationService: MealClassificationService, private toastCtrl: ToastController) {
        this.mealClassification = new MealClassification();
        this.mealClassification.id = params.get('id');
    }

    ionViewDidLoad() {
        this.mealClassificationService.find(this.mealClassification.id).subscribe(data => this.mealClassification = data);
    }

    open(item: MealClassification) {
        let modal = this.modalCtrl.create('MealClassificationDialogPage', {item: item});
        modal.onDidDismiss(mealClassification => {
            if (mealClassification) {
                this.mealClassificationService.update(mealClassification).subscribe(data => {
                    this.mealClassification = data;
                    let toast = this.toastCtrl.create(
                        {message: 'MealClassification updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
