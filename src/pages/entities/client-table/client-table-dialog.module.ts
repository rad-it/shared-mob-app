import { SectorService } from '../sector';
import { ClientTableStatusService } from '../client-table-status';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTableDialogPage } from './client-table-dialog';
import { ClientTableService } from './client-table.provider';

@NgModule({
    declarations: [
        ClientTableDialogPage
    ],
    imports: [
        IonicPageModule.forChild(ClientTableDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientTableDialogPage
    ],
    providers: [
        ClientTableService,
        SectorService,
        ClientTableStatusService,
    ]
})
export class ClientTableDialogPageModule {
}
