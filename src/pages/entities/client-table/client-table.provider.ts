import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';
// todo: handle dates

import { ClientTable } from '../../../models/entities/client-table.model';

@Injectable()
export class ClientTableService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/client-tables';

    constructor(private http: HttpClient) { }

    create(clientTable: ClientTable): Observable<ClientTable> {
        return this.http.post(this.resourceUrl, clientTable);
    }

    update(clientTable: ClientTable): Observable<ClientTable> {
        return this.http.put(this.resourceUrl, clientTable);
    }

    find(id: number): Observable<ClientTable> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
