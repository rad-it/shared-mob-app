import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTablePage } from './client-table';
import { ClientTableService } from './client-table.provider';

@NgModule({
    declarations: [
        ClientTablePage
    ],
    imports: [
        IonicPageModule.forChild(ClientTablePage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientTablePage
    ],
    providers: [ClientTableService]
})
export class ClientTablePageModule {
}
