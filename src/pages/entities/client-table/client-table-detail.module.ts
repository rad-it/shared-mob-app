import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTableDetailPage } from './client-table-detail';
import { ClientTableService } from './client-table.provider';

@NgModule({
    declarations: [
        ClientTableDetailPage
    ],
    imports: [
        IonicPageModule.forChild(ClientTableDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientTableDetailPage
    ],
    providers: [ClientTableService]
})
export class ClientTableDetailPageModule {
}
