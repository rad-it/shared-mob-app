export * from '../../../models/entities/client-table.model';
export * from './client-table.provider';
export * from './client-table-dialog';
export * from './client-table-detail';
export * from './client-table';
