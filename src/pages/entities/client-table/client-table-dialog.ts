import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { ClientTable } from '../../../models/entities/client-table.model';
import { ClientTableService } from './client-table.provider';
import { Sector, SectorService } from '../sector';
import { ClientTableStatus, ClientTableStatusService } from '../client-table-status';

@IonicPage()
@Component({
    selector: 'page-client-table-dialog',
    templateUrl: 'client-table-dialog.html'
})
export class ClientTableDialogPage {

    clientTable: ClientTable;
    sectors: Sector[];
    clienttablestatuses: ClientTableStatus[];
    deleteDate: string;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private sectorService: SectorService,
                private clientTableStatusService: ClientTableStatusService,
                private clientTableService: ClientTableService) {
        this.clientTable = params.get('item');
        if (this.clientTable && this.clientTable.id) {
            this.clientTableService.find(this.clientTable.id).subscribe(data => {
                this.clientTable = data;
            });
        } else {
            this.clientTable = new ClientTable();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.clientTable.id : null],
            number: [params.get('item') ? this.clientTable.number : '',  Validators.required],
            chairs: [params.get('item') ? this.clientTable.chairs : '',  Validators.required],
            modifiable: [params.get('item') ? this.clientTable.modifiable : 'false',  Validators.required],
            deleteDate: [params.get('item') ? this.clientTable.deleteDate : '', ],
            sector: [params.get('item') ? this.clientTable.sector : '',],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
        this.sectorService.query()
            .subscribe(data => { this.sectors = data; }, (error) => this.onError(error));
        this.clientTableStatusService.query()
            .subscribe(data => { this.clienttablestatuses = data; }, (error) => this.onError(error));
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the client-table, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    compareSector(first: Sector, second: Sector): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackSectorById(index: number, item: Sector) {
        return item.id;
    }
    compareClientTableStatus(first: ClientTableStatus, second: ClientTableStatus): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackClientTableStatusById(index: number, item: ClientTableStatus) {
        return item.id;
    }
}
