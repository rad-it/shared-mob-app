import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { ClientTable } from '../../../models/entities/client-table.model';
import { ClientTableService } from './client-table.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-client-table',
    templateUrl: 'client-table.html'
})
export class ClientTablePage {
    clientTables: ClientTable[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private clientTableService: ClientTableService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.clientTables = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.clientTableService.query().subscribe(
            (response) => {
                this.clientTables = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: ClientTable) {
        return item.id;
    }

    open(slidingItem: any, item: ClientTable) {
        let modal = this.modalCtrl.create('ClientTableDialogPage', {item: item});
        modal.onDidDismiss(clientTable => {
            if (clientTable) {
                if (clientTable.id) {
                    this.clientTableService.update(clientTable).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'ClientTable updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.clientTableService.create(clientTable).subscribe(data => {
                        this.clientTables.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'ClientTable added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(clientTable) {
        this.clientTableService.delete(clientTable.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'ClientTable deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(clientTable: ClientTable) {
        this.navCtrl.push('ClientTableDetailPage', {id: clientTable.id});
    }
}
