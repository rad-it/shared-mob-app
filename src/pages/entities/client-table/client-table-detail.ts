import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { ClientTable } from '../../../models/entities/client-table.model';
import { ClientTableService } from './client-table.provider';

@IonicPage({
    segment: 'client-table-detail/:id',
    defaultHistory: ['EntityPage', 'client-tablePage']
})
@Component({
    selector: 'page-client-table-detail',
    templateUrl: 'client-table-detail.html'
})
export class ClientTableDetailPage {
    clientTable: ClientTable;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private clientTableService: ClientTableService, private toastCtrl: ToastController) {
        this.clientTable = new ClientTable();
        this.clientTable.id = params.get('id');
    }

    ionViewDidLoad() {
        this.clientTableService.find(this.clientTable.id).subscribe(data => this.clientTable = data);
    }

    open(item: ClientTable) {
        let modal = this.modalCtrl.create('ClientTableDialogPage', {item: item});
        modal.onDidDismiss(clientTable => {
            if (clientTable) {
                this.clientTableService.update(clientTable).subscribe(data => {
                    this.clientTable = data;
                    let toast = this.toastCtrl.create(
                        {message: 'ClientTable updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
