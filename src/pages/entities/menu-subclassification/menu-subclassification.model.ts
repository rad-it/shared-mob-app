import { BaseEntity } from './../../../models';

export class MenuSubclassification implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
