import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { MenuSubclassification } from './menu-subclassification.model';
import { MenuSubclassificationService } from './menu-subclassification.provider';

@IonicPage()
@Component({
    selector: 'page-menu-subclassification-dialog',
    templateUrl: 'menu-subclassification-dialog.html'
})
export class MenuSubclassificationDialogPage {

    menuSubclassification: MenuSubclassification;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private menuSubclassificationService: MenuSubclassificationService) {
        this.menuSubclassification = params.get('item');
        if (this.menuSubclassification && this.menuSubclassification.id) {
            this.menuSubclassificationService.find(this.menuSubclassification.id).subscribe(data => {
                this.menuSubclassification = data;
            });
        } else {
            this.menuSubclassification = new MenuSubclassification();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.menuSubclassification.id : null],
            name: [params.get('item') ? this.menuSubclassification.name : '',  Validators.required],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the menu-subclassification, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
