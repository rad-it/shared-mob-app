import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuSubclassificationDialogPage } from './menu-subclassification-dialog';
import { MenuSubclassificationService } from './menu-subclassification.provider';

@NgModule({
    declarations: [
        MenuSubclassificationDialogPage
    ],
    imports: [
        IonicPageModule.forChild(MenuSubclassificationDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuSubclassificationDialogPage
    ],
    providers: [
        MenuSubclassificationService
    ]
})
export class MenuSubclassificationDialogPageModule {
}
