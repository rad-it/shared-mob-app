import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuSubclassificationPage } from './menu-subclassification';
import { MenuSubclassificationService } from './menu-subclassification.provider';

@NgModule({
    declarations: [
        MenuSubclassificationPage
    ],
    imports: [
        IonicPageModule.forChild(MenuSubclassificationPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuSubclassificationPage
    ],
    providers: [MenuSubclassificationService]
})
export class MenuSubclassificationPageModule {
}
