import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { MenuSubclassification } from './menu-subclassification.model';

@Injectable()
export class MenuSubclassificationService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/menu-subclassifications';

    constructor(private http: HttpClient) { }

    create(menuSubclassification: MenuSubclassification): Observable<MenuSubclassification> {
        return this.http.post(this.resourceUrl, menuSubclassification);
    }

    update(menuSubclassification: MenuSubclassification): Observable<MenuSubclassification> {
        return this.http.put(this.resourceUrl, menuSubclassification);
    }

    find(id: number): Observable<MenuSubclassification> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
