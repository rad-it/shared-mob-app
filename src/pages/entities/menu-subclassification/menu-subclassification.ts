import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { MenuSubclassification } from './menu-subclassification.model';
import { MenuSubclassificationService } from './menu-subclassification.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-menu-subclassification',
    templateUrl: 'menu-subclassification.html'
})
export class MenuSubclassificationPage {
    menuSubclassifications: MenuSubclassification[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private menuSubclassificationService: MenuSubclassificationService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.menuSubclassifications = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.menuSubclassificationService.query().subscribe(
            (response) => {
                this.menuSubclassifications = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: MenuSubclassification) {
        return item.id;
    }

    open(slidingItem: any, item: MenuSubclassification) {
        let modal = this.modalCtrl.create('MenuSubclassificationDialogPage', {item: item});
        modal.onDidDismiss(menuSubclassification => {
            if (menuSubclassification) {
                if (menuSubclassification.id) {
                    this.menuSubclassificationService.update(menuSubclassification).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'MenuSubclassification updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.menuSubclassificationService.create(menuSubclassification).subscribe(data => {
                        this.menuSubclassifications.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'MenuSubclassification added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(menuSubclassification) {
        this.menuSubclassificationService.delete(menuSubclassification.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'MenuSubclassification deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(menuSubclassification: MenuSubclassification) {
        this.navCtrl.push('MenuSubclassificationDetailPage', {id: menuSubclassification.id});
    }
}
