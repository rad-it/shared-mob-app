import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { MenuSubclassification } from './menu-subclassification.model';
import { MenuSubclassificationService } from './menu-subclassification.provider';

@IonicPage({
    segment: 'menu-subclassification-detail/:id',
    defaultHistory: ['EntityPage', 'menu-subclassificationPage']
})
@Component({
    selector: 'page-menu-subclassification-detail',
    templateUrl: 'menu-subclassification-detail.html'
})
export class MenuSubclassificationDetailPage {
    menuSubclassification: MenuSubclassification;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private menuSubclassificationService: MenuSubclassificationService, private toastCtrl: ToastController) {
        this.menuSubclassification = new MenuSubclassification();
        this.menuSubclassification.id = params.get('id');
    }

    ionViewDidLoad() {
        this.menuSubclassificationService.find(this.menuSubclassification.id).subscribe(data => this.menuSubclassification = data);
    }

    open(item: MenuSubclassification) {
        let modal = this.modalCtrl.create('MenuSubclassificationDialogPage', {item: item});
        modal.onDidDismiss(menuSubclassification => {
            if (menuSubclassification) {
                this.menuSubclassificationService.update(menuSubclassification).subscribe(data => {
                    this.menuSubclassification = data;
                    let toast = this.toastCtrl.create(
                        {message: 'MenuSubclassification updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
