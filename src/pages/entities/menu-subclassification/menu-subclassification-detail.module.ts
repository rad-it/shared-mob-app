import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuSubclassificationDetailPage } from './menu-subclassification-detail';
import { MenuSubclassificationService } from './menu-subclassification.provider';

@NgModule({
    declarations: [
        MenuSubclassificationDetailPage
    ],
    imports: [
        IonicPageModule.forChild(MenuSubclassificationDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuSubclassificationDetailPage
    ],
    providers: [MenuSubclassificationService]
})
export class MenuSubclassificationDetailPageModule {
}
