import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTableStatusDialogPage } from './client-table-status-dialog';
import { ClientTableStatusService } from './client-table-status.provider';

@NgModule({
    declarations: [
        ClientTableStatusDialogPage
    ],
    imports: [
        IonicPageModule.forChild(ClientTableStatusDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientTableStatusDialogPage
    ],
    providers: [
        ClientTableStatusService
    ]
})
export class ClientTableStatusDialogPageModule {
}
