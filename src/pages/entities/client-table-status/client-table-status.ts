import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { ClientTableStatus } from './client-table-status.model';
import { ClientTableStatusService } from './client-table-status.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-client-table-status',
    templateUrl: 'client-table-status.html'
})
export class ClientTableStatusPage {
    clientTableStatuses: ClientTableStatus[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private clientTableStatusService: ClientTableStatusService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.clientTableStatuses = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.clientTableStatusService.query().subscribe(
            (response) => {
                this.clientTableStatuses = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: ClientTableStatus) {
        return item.id;
    }

    open(slidingItem: any, item: ClientTableStatus) {
        let modal = this.modalCtrl.create('ClientTableStatusDialogPage', {item: item});
        modal.onDidDismiss(clientTableStatus => {
            if (clientTableStatus) {
                if (clientTableStatus.id) {
                    this.clientTableStatusService.update(clientTableStatus).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'ClientTableStatus updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.clientTableStatusService.create(clientTableStatus).subscribe(data => {
                        this.clientTableStatuses.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'ClientTableStatus added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(clientTableStatus) {
        this.clientTableStatusService.delete(clientTableStatus.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'ClientTableStatus deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(clientTableStatus: ClientTableStatus) {
        this.navCtrl.push('ClientTableStatusDetailPage', {id: clientTableStatus.id});
    }
}
