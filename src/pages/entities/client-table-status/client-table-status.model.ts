import { BaseEntity } from './../../../models';

export class ClientTableStatus implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
