import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { ClientTableStatus } from './client-table-status.model';
import { ClientTableStatusService } from './client-table-status.provider';

@IonicPage()
@Component({
    selector: 'page-client-table-status-dialog',
    templateUrl: 'client-table-status-dialog.html'
})
export class ClientTableStatusDialogPage {

    clientTableStatus: ClientTableStatus;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private clientTableStatusService: ClientTableStatusService) {
        this.clientTableStatus = params.get('item');
        if (this.clientTableStatus && this.clientTableStatus.id) {
            this.clientTableStatusService.find(this.clientTableStatus.id).subscribe(data => {
                this.clientTableStatus = data;
            });
        } else {
            this.clientTableStatus = new ClientTableStatus();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.clientTableStatus.id : null],
            name: [params.get('item') ? this.clientTableStatus.name : '',  Validators.required],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the client-table-status, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
