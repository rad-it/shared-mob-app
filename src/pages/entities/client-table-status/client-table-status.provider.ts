import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { ClientTableStatus } from './client-table-status.model';

@Injectable()
export class ClientTableStatusService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/client-table-statuses';

    constructor(private http: HttpClient) { }

    create(clientTableStatus: ClientTableStatus): Observable<ClientTableStatus> {
        return this.http.post(this.resourceUrl, clientTableStatus);
    }

    update(clientTableStatus: ClientTableStatus): Observable<ClientTableStatus> {
        return this.http.put(this.resourceUrl, clientTableStatus);
    }

    find(id: number): Observable<ClientTableStatus> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
