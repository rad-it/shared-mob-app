import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTableStatusDetailPage } from './client-table-status-detail';
import { ClientTableStatusService } from './client-table-status.provider';

@NgModule({
    declarations: [
        ClientTableStatusDetailPage
    ],
    imports: [
        IonicPageModule.forChild(ClientTableStatusDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientTableStatusDetailPage
    ],
    providers: [ClientTableStatusService]
})
export class ClientTableStatusDetailPageModule {
}
