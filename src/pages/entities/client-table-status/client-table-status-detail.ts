import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { ClientTableStatus } from './client-table-status.model';
import { ClientTableStatusService } from './client-table-status.provider';

@IonicPage({
    segment: 'client-table-status-detail/:id',
    defaultHistory: ['EntityPage', 'client-table-statusPage']
})
@Component({
    selector: 'page-client-table-status-detail',
    templateUrl: 'client-table-status-detail.html'
})
export class ClientTableStatusDetailPage {
    clientTableStatus: ClientTableStatus;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private clientTableStatusService: ClientTableStatusService, private toastCtrl: ToastController) {
        this.clientTableStatus = new ClientTableStatus();
        this.clientTableStatus.id = params.get('id');
    }

    ionViewDidLoad() {
        this.clientTableStatusService.find(this.clientTableStatus.id).subscribe(data => this.clientTableStatus = data);
    }

    open(item: ClientTableStatus) {
        let modal = this.modalCtrl.create('ClientTableStatusDialogPage', {item: item});
        modal.onDidDismiss(clientTableStatus => {
            if (clientTableStatus) {
                this.clientTableStatusService.update(clientTableStatus).subscribe(data => {
                    this.clientTableStatus = data;
                    let toast = this.toastCtrl.create(
                        {message: 'ClientTableStatus updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
