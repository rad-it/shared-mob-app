import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTableStatusPage } from './client-table-status';
import { ClientTableStatusService } from './client-table-status.provider';

@NgModule({
    declarations: [
        ClientTableStatusPage
    ],
    imports: [
        IonicPageModule.forChild(ClientTableStatusPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientTableStatusPage
    ],
    providers: [ClientTableStatusService]
})
export class ClientTableStatusPageModule {
}
