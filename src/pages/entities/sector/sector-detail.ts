import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { Sector } from '../../../models/entities/sector.model';
import { SectorService } from './sector.provider';

@IonicPage({
    segment: 'sector-detail/:id',
    defaultHistory: ['EntityPage', 'sectorPage']
})
@Component({
    selector: 'page-sector-detail',
    templateUrl: 'sector-detail.html'
})
export class SectorDetailPage {
    sector: Sector;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private sectorService: SectorService, private toastCtrl: ToastController) {
        this.sector = new Sector();
        this.sector.id = params.get('id');
    }

    ionViewDidLoad() {
        this.sectorService.find(this.sector.id).subscribe(data => this.sector = data);
    }

    open(item: Sector) {
        let modal = this.modalCtrl.create('SectorDialogPage', {item: item});
        modal.onDidDismiss(sector => {
            if (sector) {
                this.sectorService.update(sector).subscribe(data => {
                    this.sector = data;
                    let toast = this.toastCtrl.create(
                        {message: 'Sector updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
