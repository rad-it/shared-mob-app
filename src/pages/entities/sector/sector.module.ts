import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SectorPage } from './sector';
import { SectorService } from './sector.provider';

@NgModule({
    declarations: [
        SectorPage
    ],
    imports: [
        IonicPageModule.forChild(SectorPage),
        TranslateModule.forChild()
    ],
    exports: [
        SectorPage
    ],
    providers: [SectorService]
})
export class SectorPageModule {
}
