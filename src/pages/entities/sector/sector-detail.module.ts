import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SectorDetailPage } from './sector-detail';
import { SectorService } from './sector.provider';

@NgModule({
    declarations: [
        SectorDetailPage
    ],
    imports: [
        IonicPageModule.forChild(SectorDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        SectorDetailPage
    ],
    providers: [SectorService]
})
export class SectorDetailPageModule {
}
