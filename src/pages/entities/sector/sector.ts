import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Sector } from '../../../models/entities/sector.model';
import { SectorService } from './sector.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-sector',
    templateUrl: 'sector.html'
})
export class SectorPage {
    sectors: Sector[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private sectorService: SectorService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.sectors = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.sectorService.query().subscribe(
            (response) => {
                this.sectors = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Sector) {
        return item.id;
    }

    open(slidingItem: any, item: Sector) {
        let modal = this.modalCtrl.create('SectorDialogPage', {item: item});
        modal.onDidDismiss(sector => {
            if (sector) {
                if (sector.id) {
                    this.sectorService.update(sector).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Sector updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.sectorService.create(sector).subscribe(data => {
                        this.sectors.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Sector added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(sector) {
        this.sectorService.delete(sector.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Sector deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(sector: Sector) {
        this.navCtrl.push('SectorDetailPage', {id: sector.id});
    }
}
