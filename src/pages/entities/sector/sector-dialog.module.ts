import { RestaurantService } from '../restaurant';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SectorDialogPage } from './sector-dialog';
import { SectorService } from './sector.provider';

@NgModule({
    declarations: [
        SectorDialogPage
    ],
    imports: [
        IonicPageModule.forChild(SectorDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        SectorDialogPage
    ],
    providers: [
        SectorService,
        RestaurantService,
    ]
})
export class SectorDialogPageModule {
}
