import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Sector } from '../../../models/entities/sector.model';
import { SectorService } from './sector.provider';
import { Restaurant, RestaurantService } from '../restaurant';

@IonicPage()
@Component({
    selector: 'page-sector-dialog',
    templateUrl: 'sector-dialog.html'
})
export class SectorDialogPage {

    sector: Sector;
    restaurants: Restaurant[];
    deleteDate: string;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private restaurantService: RestaurantService,
                private sectorService: SectorService) {
        this.sector = params.get('item');
        if (this.sector && this.sector.id) {
            this.sectorService.find(this.sector.id).subscribe(data => {
                this.sector = data;
            });
        } else {
            this.sector = new Sector();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.sector.id : null],
            name: [params.get('item') ? this.sector.name : '',  Validators.required],
            accesibility: [params.get('item') ? this.sector.accesibility : 'false',  Validators.required],
            deleteDate: [params.get('item') ? this.sector.deleteDate : '', ],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
        this.restaurantService.query()
            .subscribe(data => { this.restaurants = data; }, (error) => this.onError(error));
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the sector, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    compareRestaurant(first: Restaurant, second: Restaurant): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackRestaurantById(index: number, item: Restaurant) {
        return item.id;
    }
}
