import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';
// todo: handle dates

import { Sector } from '../../../models/entities/sector.model';

@Injectable()
export class SectorService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/sectors';

    constructor(private http: HttpClient) { }

    create(sector: Sector): Observable<Sector> {
        return this.http.post(this.resourceUrl, sector);
    }

    update(sector: Sector): Observable<Sector> {
        return this.http.put(this.resourceUrl, sector);
    }

    find(id: number): Observable<Sector> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
