import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage({
  defaultHistory: ['HomePage']
})
@Component({
  selector: 'page-entity',
  templateUrl: 'entity.html'
})
export class EntityPage {

  entities: Array<any> = [
      {name: 'Restaurant', component: 'RestaurantPage'},
      {name: 'Location', component: 'LocationPage'},
      {name: 'RestaurantType', component: 'RestaurantTypePage'},
      {name: 'MealClassification', component: 'MealClassificationPage'},
      {name: 'Picture', component: 'PicturePage'},
      {name: 'MenuClassification', component: 'MenuClassificationPage'},
      {name: 'MenuSubclassification', component: 'MenuSubclassificationPage'},
      {name: 'MeasuringUnit', component: 'MeasuringUnitPage'},
      {name: 'Stay', component: 'StayPage'},
      {name: 'Booking', component: 'BookingPage'},
      {name: 'StayOrder', component: 'StayOrderPage'},
      {name: 'Menu', component: 'MenuPage'},
      {name: 'Client', component: 'ClientPage'},
      {name: 'Nationality', component: 'NationalityPage'},
      {name: 'ClientTable', component: 'ClientTablePage'},
      {name: 'ClientTableStatus', component: 'ClientTableStatusPage'},
      {name: 'Sector', component: 'SectorPage'},
      {name: 'QrStay', component: 'QrStayPage'}
      /* jhipster-needle-add-entity-page - JHipster will add entity pages here */
  ];

  constructor(public nav: NavController) { }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }

  ionViewWillLoad() {
    let page = location.hash.substring(location.hash.lastIndexOf('/') + 1);
    let urlParts = location.hash.split('/');
    page = page.charAt(0).toUpperCase() + page.substring(1) + 'Page';
    let destination;
    this.entities.forEach(entity => {
      if (entity.component === page) {
        destination = entity.component;
      }
    });
    if (destination) {
      this.nav.push(destination);
    } else if (urlParts.length === 5) {
      // convert from URL to page name: foo-detail to FooDetailPage
      const detailPage = this.urlToTitleCase(urlParts[3]) + 'Page';
      this.nav.push(detailPage, {id: urlParts[4]})
    }
  }

  private urlToTitleCase(str) {
    return str.replace(/(-|^)([^-]?)/g, (_, prep, letter) => {
      return (prep && '') + letter.toUpperCase();
    });
  }
}
