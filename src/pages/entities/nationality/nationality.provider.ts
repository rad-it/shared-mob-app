import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { Nationality } from './nationality.model';

@Injectable()
export class NationalityService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/nationalities';

    constructor(private http: HttpClient) { }

    create(nationality: Nationality): Observable<Nationality> {
        return this.http.post(this.resourceUrl, nationality);
    }

    update(nationality: Nationality): Observable<Nationality> {
        return this.http.put(this.resourceUrl, nationality);
    }

    find(id: number): Observable<Nationality> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    getAll(): Observable<any> {
        return this.http.get(`${this.resourceUrl}?size=500`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }

}
