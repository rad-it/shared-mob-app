import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NationalityPage } from './nationality';
import { NationalityService } from './nationality.provider';

@NgModule({
    declarations: [
        NationalityPage
    ],
    imports: [
        IonicPageModule.forChild(NationalityPage),
        TranslateModule.forChild()
    ],
    exports: [
        NationalityPage
    ],
    providers: [NationalityService]
})
export class NationalityPageModule {
}
