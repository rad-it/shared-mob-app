import { PictureService } from '../picture';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NationalityDialogPage } from './nationality-dialog';
import { NationalityService } from './nationality.provider';

@NgModule({
    declarations: [
        NationalityDialogPage
    ],
    imports: [
        IonicPageModule.forChild(NationalityDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        NationalityDialogPage
    ],
    providers: [
        NationalityService,
        PictureService,
    ]
})
export class NationalityDialogPageModule {
}
