import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Nationality } from './nationality.model';
import { NationalityService } from './nationality.provider';
import { Picture, PictureService } from '../picture';

@IonicPage()
@Component({
    selector: 'page-nationality-dialog',
    templateUrl: 'nationality-dialog.html'
})
export class NationalityDialogPage {

    nationality: Nationality;
    images: Picture[];
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private pictureService: PictureService,
                private nationalityService: NationalityService) {
        this.nationality = params.get('item');
        if (this.nationality && this.nationality.id) {
            this.nationalityService.find(this.nationality.id).subscribe(data => {
                this.nationality = data;
            });
        } else {
            this.nationality = new Nationality();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.nationality.id : null],
            nameSp: [params.get('item') ? this.nationality.nameSp : '', ],
            nameEn: [params.get('item') ? this.nationality.nameEn : '', ],
            name: [params.get('item') ? this.nationality.name : '', ],
            iso2: [params.get('item') ? this.nationality.iso2 : '', ],
            iso3: [params.get('item') ? this.nationality.iso3 : '', ],
            phoneCode: [params.get('item') ? this.nationality.phoneCode : '', ],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the nationality, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    comparePicture(first: Picture, second: Picture): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackPictureById(index: number, item: Picture) {
        return item.id;
    }
}
