import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Nationality } from './nationality.model';
import { NationalityService } from './nationality.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-nationality',
    templateUrl: 'nationality.html'
})
export class NationalityPage {
    nationalities: Nationality[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private nationalityService: NationalityService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.nationalities = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.nationalityService.query().subscribe(
            (response) => {
                this.nationalities = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Nationality) {
        return item.id;
    }

    open(slidingItem: any, item: Nationality) {
        let modal = this.modalCtrl.create('NationalityDialogPage', {item: item});
        modal.onDidDismiss(nationality => {
            if (nationality) {
                if (nationality.id) {
                    this.nationalityService.update(nationality).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Nationality updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.nationalityService.create(nationality).subscribe(data => {
                        this.nationalities.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Nationality added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(nationality) {
        this.nationalityService.delete(nationality.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Nationality deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(nationality: Nationality) {
        this.navCtrl.push('NationalityDetailPage', {id: nationality.id});
    }
}
