import { BaseEntity } from './../../../models';

export class Nationality implements BaseEntity {
    constructor(
        public id?: number,
        public nameSp?: string,
        public nameEn?: string,
        public name?: string,
        public iso2?: string,
        public iso3?: string,
        public phoneCode?: string,
        public imageId?: number,
    ) {
    }
}
