import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NationalityDetailPage } from './nationality-detail';
import { NationalityService } from './nationality.provider';

@NgModule({
    declarations: [
        NationalityDetailPage
    ],
    imports: [
        IonicPageModule.forChild(NationalityDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        NationalityDetailPage
    ],
    providers: [NationalityService]
})
export class NationalityDetailPageModule {
}
