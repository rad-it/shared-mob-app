import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { Nationality } from './nationality.model';
import { NationalityService } from './nationality.provider';

@IonicPage({
    segment: 'nationality-detail/:id',
    defaultHistory: ['EntityPage', 'nationalityPage']
})
@Component({
    selector: 'page-nationality-detail',
    templateUrl: 'nationality-detail.html'
})
export class NationalityDetailPage {
    nationality: Nationality;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private nationalityService: NationalityService, private toastCtrl: ToastController) {
        this.nationality = new Nationality();
        this.nationality.id = params.get('id');
    }

    ionViewDidLoad() {
        this.nationalityService.find(this.nationality.id).subscribe(data => this.nationality = data);
    }

    open(item: Nationality) {
        let modal = this.modalCtrl.create('NationalityDialogPage', {item: item});
        modal.onDidDismiss(nationality => {
            if (nationality) {
                this.nationalityService.update(nationality).subscribe(data => {
                    this.nationality = data;
                    let toast = this.toastCtrl.create(
                        {message: 'Nationality updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
