import { BaseEntity } from './../../../models';

export class MenuClassification implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public subclassifications?: BaseEntity[],
    ) {
    }
}
