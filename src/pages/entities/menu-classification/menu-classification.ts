import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { MenuClassification } from './menu-classification.model';
import { MenuClassificationService } from './menu-classification.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-menu-classification',
    templateUrl: 'menu-classification.html'
})
export class MenuClassificationPage {
    menuClassifications: MenuClassification[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private menuClassificationService: MenuClassificationService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.menuClassifications = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.menuClassificationService.query().subscribe(
            (response) => {
                this.menuClassifications = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: MenuClassification) {
        return item.id;
    }

    open(slidingItem: any, item: MenuClassification) {
        let modal = this.modalCtrl.create('MenuClassificationDialogPage', {item: item});
        modal.onDidDismiss(menuClassification => {
            if (menuClassification) {
                if (menuClassification.id) {
                    this.menuClassificationService.update(menuClassification).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'MenuClassification updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.menuClassificationService.create(menuClassification).subscribe(data => {
                        this.menuClassifications.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'MenuClassification added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(menuClassification) {
        this.menuClassificationService.delete(menuClassification.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'MenuClassification deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(menuClassification: MenuClassification) {
        this.navCtrl.push('MenuClassificationDetailPage', {id: menuClassification.id});
    }
}
