import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { MenuClassification } from './menu-classification.model';
import { MenuClassificationService } from './menu-classification.provider';
import { MenuSubclassification, MenuSubclassificationService } from '../menu-subclassification';

@IonicPage()
@Component({
    selector: 'page-menu-classification-dialog',
    templateUrl: 'menu-classification-dialog.html'
})
export class MenuClassificationDialogPage {

    menuClassification: MenuClassification;
    menusubclassifications: MenuSubclassification[];
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private menuSubclassificationService: MenuSubclassificationService,
                private menuClassificationService: MenuClassificationService) {
        this.menuClassification = params.get('item');
        if (this.menuClassification && this.menuClassification.id) {
            this.menuClassificationService.find(this.menuClassification.id).subscribe(data => {
                this.menuClassification = data;
            });
        } else {
            this.menuClassification = new MenuClassification();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.menuClassification.id : null],
            name: [params.get('item') ? this.menuClassification.name : '',  Validators.required],
            menuSubclassifications: [params.get('item') ? this.menuClassification.subclassifications : '',],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
        this.menuSubclassificationService.query()
            .subscribe(data => { this.menusubclassifications = data; }, (error) => this.onError(error));
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the menu-classification, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    compareMenuSubclassification(first: MenuSubclassification, second: MenuSubclassification): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMenuSubclassificationById(index: number, item: MenuSubclassification) {
        return item.id;
    }
}
