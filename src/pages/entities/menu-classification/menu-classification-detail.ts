import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { MenuClassification } from './menu-classification.model';
import { MenuClassificationService } from './menu-classification.provider';

@IonicPage({
    segment: 'menu-classification-detail/:id',
    defaultHistory: ['EntityPage', 'menu-classificationPage']
})
@Component({
    selector: 'page-menu-classification-detail',
    templateUrl: 'menu-classification-detail.html'
})
export class MenuClassificationDetailPage {
    menuClassification: MenuClassification;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private menuClassificationService: MenuClassificationService, private toastCtrl: ToastController) {
        this.menuClassification = new MenuClassification();
        this.menuClassification.id = params.get('id');
    }

    ionViewDidLoad() {
        this.menuClassificationService.find(this.menuClassification.id).subscribe(data => this.menuClassification = data);
    }

    open(item: MenuClassification) {
        let modal = this.modalCtrl.create('MenuClassificationDialogPage', {item: item});
        modal.onDidDismiss(menuClassification => {
            if (menuClassification) {
                this.menuClassificationService.update(menuClassification).subscribe(data => {
                    this.menuClassification = data;
                    let toast = this.toastCtrl.create(
                        {message: 'MenuClassification updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
