import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { MenuClassification } from './menu-classification.model';

@Injectable()
export class MenuClassificationService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/menu-classifications';

    constructor(private http: HttpClient) { }

    create(menuClassification: MenuClassification): Observable<MenuClassification> {
        return this.http.post(this.resourceUrl, menuClassification);
    }

    update(menuClassification: MenuClassification): Observable<MenuClassification> {
        return this.http.put(this.resourceUrl, menuClassification);
    }

    find(id: number): Observable<MenuClassification> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
