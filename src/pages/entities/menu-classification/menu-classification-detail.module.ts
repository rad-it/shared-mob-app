import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuClassificationDetailPage } from './menu-classification-detail';
import { MenuClassificationService } from './menu-classification.provider';

@NgModule({
    declarations: [
        MenuClassificationDetailPage
    ],
    imports: [
        IonicPageModule.forChild(MenuClassificationDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuClassificationDetailPage
    ],
    providers: [MenuClassificationService]
})
export class MenuClassificationDetailPageModule {
}
