import { MenuSubclassificationService } from '../menu-subclassification';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuClassificationDialogPage } from './menu-classification-dialog';
import { MenuClassificationService } from './menu-classification.provider';

@NgModule({
    declarations: [
        MenuClassificationDialogPage
    ],
    imports: [
        IonicPageModule.forChild(MenuClassificationDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuClassificationDialogPage
    ],
    providers: [
        MenuClassificationService,
        MenuSubclassificationService,
    ]
})
export class MenuClassificationDialogPageModule {
}
