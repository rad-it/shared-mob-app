import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuClassificationPage } from './menu-classification';
import { MenuClassificationService } from './menu-classification.provider';

@NgModule({
    declarations: [
        MenuClassificationPage
    ],
    imports: [
        IonicPageModule.forChild(MenuClassificationPage),
        TranslateModule.forChild()
    ],
    exports: [
        MenuClassificationPage
    ],
    providers: [MenuClassificationService]
})
export class MenuClassificationPageModule {
}
