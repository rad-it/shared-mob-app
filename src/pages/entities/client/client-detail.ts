import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { Client } from '../../../models/entities/client.model';
import { ClientService } from '../../../providers/entities/client.provider';

@IonicPage({
    segment: 'client-detail/:id',
    defaultHistory: ['EntityPage', 'clientPage']
})
@Component({
    selector: 'page-client-detail',
    templateUrl: 'client-detail.html'
})
export class ClientDetailPage {
    client: Client;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private clientService: ClientService, private toastCtrl: ToastController) {
        this.client = new Client();
        this.client.id = params.get('id');
    }

    ionViewDidLoad() {
        this.clientService.find(this.client.id).subscribe(data => this.client = data);
    }

    open(item: Client) {
        let modal = this.modalCtrl.create('ClientDialogPage', {item: item});
        modal.onDidDismiss(client => {
            if (client) {
                this.clientService.update(client).subscribe(data => {
                    this.client = data;
                    let toast = this.toastCtrl.create(
                        {message: 'Client updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
