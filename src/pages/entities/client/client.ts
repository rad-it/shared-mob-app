import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Client } from '../../../models/entities/client.model';
import { ClientService } from '../../../providers/entities/client.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-client',
    templateUrl: 'client.html'
})
export class ClientPage {
    clients: Client[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private clientService: ClientService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.clients = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.clientService.query().subscribe(
            (response) => {
                this.clients = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Client) {
        return item.id;
    }

    open(slidingItem: any, item: Client) {
        let modal = this.modalCtrl.create('ClientDialogPage', {item: item});
        modal.onDidDismiss(client => {
            if (client) {
                if (client.id) {
                    this.clientService.update(client).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Client updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.clientService.create(client).subscribe(data => {
                        this.clients.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Client added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(client) {
        this.clientService.delete(client.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Client deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(client: Client) {
        this.navCtrl.push('ClientDetailPage', {id: client.id});
    }
}
