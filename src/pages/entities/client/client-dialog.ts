import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Client } from '../../../models/entities/client.model';
import { ClientService } from '../../../providers/entities/client.provider';
import { Location, LocationService } from '../location';

@IonicPage()
@Component({
    selector: 'page-client-dialog',
    templateUrl: 'client-dialog.html'
})
export class ClientDialogPage {

    client: Client;
    locations: Location[];
    birthdate: string;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private locationService: LocationService,
                private clientService: ClientService) {
        this.client = params.get('item');
        if (this.client && this.client.id) {
            this.clientService.find(this.client.id).subscribe(data => {
                this.client = data;
            });
        } else {
            this.client = new Client();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.client.id : null],
            birthdate: [params.get('item') ? this.client.birthdate : '', ],
            userId: [params.get('item') ? this.client.userId : '', ],
            location: [params.get('item') ? this.client.location : '',],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
        this.locationService
            .query({filter: 'client-is-null'})
            .subscribe(data => {
                if (!this.client.locationId) {
                    this.locations = data;
                } else {
                    this.locationService
                        .find(this.client.locationId)
                        .subscribe((subData: Location) => {
                            this.locations = [subData].concat(subData);
                        }, (error) => this.onError(error));
                }
            }, (error) => this.onError(error));
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the client, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    compareLocation(first: Location, second: Location): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackLocationById(index: number, item: Location) {
        return item.id;
    }
}
