import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientPage } from './client';
import { ClientService } from '../../../providers/entities/client.provider';

@NgModule({
    declarations: [
        ClientPage
    ],
    imports: [
        IonicPageModule.forChild(ClientPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientPage
    ],
    providers: [ClientService]
})
export class ClientPageModule {
}
