export * from '../../../models/entities/client.model';
export * from '../../../providers/entities/client.provider';
export * from './client-dialog';
export * from './client-detail';
export * from './client';
