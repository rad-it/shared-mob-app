import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientDetailPage } from './client-detail';
import { ClientService } from '../../../providers/entities/client.provider';

@NgModule({
    declarations: [
        ClientDetailPage
    ],
    imports: [
        IonicPageModule.forChild(ClientDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientDetailPage
    ],
    providers: [ClientService]
})
export class ClientDetailPageModule {
}
