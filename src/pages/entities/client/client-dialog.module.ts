import { LocationService } from '../location';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientDialogPage } from './client-dialog';
import { ClientService } from '../../../providers/entities/client.provider';

@NgModule({
    declarations: [
        ClientDialogPage
    ],
    imports: [
        IonicPageModule.forChild(ClientDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        ClientDialogPage
    ],
    providers: [
        ClientService,
        LocationService,
    ]
})
export class ClientDialogPageModule {
}
