import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ViewController, ToastController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Booking } from "../../models/entities/booking.model";
import { User } from '../../providers/providers';
import { Tab3Root } from '../pages';

import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the signup form
  account: { login: string, email: string, firstName: string, lastName: string, password: string, langKey: string } = {
    login: '',
    email: '',
    firstName: '',
    lastName: '',
    password: '',
    langKey: 'es'
  };
  repeat_password: string = '';
  public form: FormGroup;
  public booking: Booking;
  // Our translated text strings
  private signupErrorString: string;
  private signupSuccessString: string;
  private existingUserError: string;
  private invalidPasswordError: string;
  private differentPasswords: string;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              public user: User,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              private fb: FormBuilder,
              params: NavParams) {

    this.translateService.get(['SIGNUP_ERROR', 'SIGNUP_SUCCESS',
      'EXISTING_USER_ERROR', 'INVALID_PASSWORD_ERROR', 'DIFFERENT_PASSWORDS']).subscribe((values) => {
      this.signupErrorString = values.SIGNUP_ERROR;
      this.signupSuccessString = values.SIGNUP_SUCCESS;
      this.existingUserError = values.EXISTING_USER_ERROR;
      this.invalidPasswordError = values.INVALID_PASSWORD_ERROR;
      this.differentPasswords = values.DIFFERENT_PASSWORDS;
    });

    this.form = this.fb.group({
      login: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
      repeatPassword: ['', Validators.compose([Validators.required])]
    });

    if(params.get('booking')) this.booking = params.get('booking');
  }

  /**
   *
   * @param form
   * @returns {Promise<any>}
   */
  doSignup(form) {
    // set login to same as email
    this.account.login = this.account.email;
    // Attempt to login in through our User service

    this.account.login = form.login;
    this.account.email = form.email;
    this.account.password = form.password;

    //Validation
    if (!_.isEqual(this.account.password, this.repeat_password)
      && !_.isEqual(this.account.password, '')) {
      let toast = this.toastCtrl.create({
        message: this.differentPasswords,
        duration: 3000,
        position: 'top'
      });
      return toast.present();
    }

    this.user.signup(this.account).subscribe((response) => {


      if(this.booking) {
        this.viewCtrl.dismiss();
      } else {
        let toast = this.toastCtrl.create({
          message: this.signupSuccessString,
          duration: 3000,
          position: 'top'
        });
        toast.present();
        this.navCtrl.setRoot( Tab3Root , {});
      }

    }, (response) => {
      // Unable to sign up
      const error = JSON.parse(response.error);
      let displayError = this.signupErrorString;
      if (response.status === 400 && error.type.includes('already-used')) {
          displayError = this.existingUserError;
      } else if (response.status === 400 && error.message === 'error.validation'
          && error.fieldErrors[0].field === 'password' && error.fieldErrors[0].message === 'Size') {
          displayError = this.invalidPasswordError;
      }
      let toast = this.toastCtrl.create({
          message: displayError,
          duration: 3000,
          position: 'top'
      });
      toast.present();
    });
  }

  /**
   *
   */
  login() {
    this.navCtrl.push('LoginPage');
  }

}
