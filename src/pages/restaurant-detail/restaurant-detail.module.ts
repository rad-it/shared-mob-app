import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantDetailsPage } from './restaurant-detail';
import { ComponentsModule } from '../../components/components.module';


//Services
import { RestaurantService } from '../../providers/entities/restaurant.provider';
import { BookingService } from '../../providers/entities/booking.provider';
import { StayService } from '../../providers/entities/stay.provider';

@NgModule({
    declarations: [
      RestaurantDetailsPage
    ],
    imports: [
      IonicPageModule.forChild(RestaurantDetailsPage),
      TranslateModule.forChild(),
      ComponentsModule
    ],
    exports: [
      RestaurantDetailsPage
    ],
    providers: [
      RestaurantService,
      BookingService,
      StayService
    ]
})
export class RestaurantDetailPageModule {
}
