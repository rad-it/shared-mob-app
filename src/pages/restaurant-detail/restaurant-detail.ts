import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { Restaurant } from '../../models/entities/restaurant.model';
import { Menu } from '../../models/entities/menu.model';

//Services
import { RestaurantService } from '../../providers/entities/restaurant.provider';
import { BookingService } from '../../providers/entities/booking.provider';
import { Booking } from "../../models/entities/booking.model";
import { StayService } from '../../providers/entities/stay.provider';
import { Stay } from "../../models/entities/stay.model";

//Page
import { BookingDetail } from "../pages";

@IonicPage({
    defaultHistory: ['EntityPage', 'restaurantPage']
})
@Component({
    selector: 'page-restaurant-details',
    templateUrl: 'restaurant-detail.html'
})
export class RestaurantDetailsPage {
  restaurant: Restaurant;
  booking: Booking;
  stay: Stay;
  menus: Menu[] = [];
  promotions: any[] = [];
  id: string = 'tab1';

  constructor(
    params: NavParams,
    private restaurantService: RestaurantService,
    private bookingService: BookingService,
    private stayService: StayService,
    public navCtrl: NavController) {

        this.restaurant = new Restaurant();
        this.restaurant.id = params.get('id');

        if(params.get('bookingId')) {
          this.booking = new Booking();
          this.booking.id = params.get('bookingId');
          this.bookingService.find(this.booking.id).subscribe(data => {
            this.booking = data;
          });
        }

        if(params.get('stayId')) {
          this.stay = new Stay();
          this.stay.id = params.get('stayId');
          this.stayService.find(this.stay.id).subscribe(data => {
            this.stay = data;
          });
        }

    }

  /**
   *
   */
  ionViewDidLoad() {
    this.loadData();
  }

  /**
   * Api call to refresh products
   * @param refresher
   */
  doRefresh(refresher: any) {
    this.loadData(refresher);
  }

  /**
   *
   * @param $event
   */
  loadData($event?) {
    this.restaurantService.find(this.restaurant.id).subscribe(data => {
      this.restaurant = data;
      if ($event) $event.complete();
    });
    this.restaurantService.getMenus(this.restaurant.id).subscribe(data => {
      this.menus = data;
    });
    this.restaurantService.getPromotions(this.restaurant.id).subscribe(data => this.promotions = data);

    if ($event) {
      setTimeout(() => {
        $event.complete();
      }, 5000);
    }
  }

  /**
   *
   * @param booking
   */
  newBooking(restaurant){
    let params = {restaurantId: restaurant.id};
    this.navCtrl.push(BookingDetail, params);
  }
}
