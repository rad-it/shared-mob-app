import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

//Modules
import { ComponentsModule } from '../../components/components.module';
import { NotificationPage } from './notification';

//Services
import { RestaurantService } from '../../providers/entities/restaurant.provider';
import { MealClassificationService } from '../../providers/entities/meal-classification.provider';
import { NotificationService } from '../../providers/entities/notification.provider';

@NgModule({
  declarations: [
    NotificationPage
  ],
  imports: [
    IonicPageModule.forChild(NotificationPage),
    TranslateModule.forChild(),
    ComponentsModule
  ],
  exports: [
    NotificationPage
  ],
  providers: [
    RestaurantService,
    Geolocation,
    MealClassificationService,
    NotificationService,
  ]
})
export class NotificationPageModule { }
