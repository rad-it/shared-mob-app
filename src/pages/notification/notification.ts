import { Component, OnInit } from '@angular/core';
import {
  App,
  IonicPage,
  NavController,
  ModalController,
  ToastController,
  LoadingController,
  Platform } from 'ionic-angular';
import { Principal } from '../../providers/auth/principal.service';
import { Geolocation } from '@ionic-native/geolocation';
import { tap } from 'rxjs/operators';
import { FcmProvider } from '../../providers/fcm/fcm';
import { MainPage, Tab1Root, Tab2Root, BookingDetail } from '../pages';
import { Events } from 'ionic-angular';
import { ItemSliding } from 'ionic-angular';

//Services
import { RestaurantService } from '../../providers/entities/restaurant.provider';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../providers/entities/notification.provider';
import { LocalStorageService } from 'ngx-webstorage';

//Pages
import { RestaurantDetails } from "../pages";

//Models
import { Restaurant } from '../../models/entities/restaurant.model';
import { Account } from "../../models/account.model";
import { Notification } from "../../models/entities/notification.model";

import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'notification.html',
  styleUrls: ["/pages/notification/notification.scss"]
})
export class NotificationPage implements OnInit {
  account: Account;
  restaurants: Restaurant[] = [];
  filter: any = {};
  getErrorString:string = '';
  view: string;
  notifications: Notification[] = [];
  notificationsPrevious: Notification[];
  notificationsRecent: Notification[];
  restaurantSelected: Restaurant;
  loaded: boolean = false;

  constructor(public navCtrl: NavController,
              private principal: Principal,
              private app: App,
              public modalCtrl: ModalController,
              public loadingCrtl: LoadingController,
              private restaurantService: RestaurantService,
              private geolocation: Geolocation,
              private toastCtrl: ToastController,
              private translateService: TranslateService,
              private notificationService: NotificationService,
              private localStorage: LocalStorageService,
              public platform: Platform,
              public events: Events,
              public fcm: FcmProvider) {
    //Filters
    this.filter.search = '';
    this.filter.page = 0;
    this.filter.size = 7;

    this.translateService.get('GET_ERROR').subscribe((value) => {
      this.getErrorString = value;
    });
  }

  ngOnInit() { 
    this.getNotification(true);
  
  }

  ionViewDidLoad(){
  }

  ionViewWillEnter(){

      this.getNotification(true);
    
  }	

  markAsRead(notification, ItemSliding): void {
    ItemSliding.close();
    notification.readDate = new Date();
    this.notificationService.markAsRead(notification.id).subscribe( (x: any) => {
      this.events.publish('notifications:unread');
    } )
}

  goToHome(notification) {
    notification.readDate = new Date();
    this.notificationService.markAsRead(notification.id).subscribe( ( x: any)   =>{
      this.navCtrl.setRoot(Tab1Root);
      this.events.publish('notifications:unread');
    })
}

goToRestaurantProfile(restaurant, notification) {
  notification.readDate = new Date();
  this.notificationService.markAsRead(notification.id).subscribe( ( x: any)   =>{
    this.navCtrl.push(RestaurantDetails, {id: restaurant.id});
    this.events.publish('notifications:unread');
  })
}

   goToBooking(restaurant, notification){
    notification.readDate = new Date();
    this.notificationService.markAsRead(notification.id).subscribe( (x: any) => {
      let params = {restaurantId: restaurant.id};
      this.navCtrl.push(BookingDetail, params);
      this.events.publish('notifications:unread');
    })
 
  }

  doRefresh(refresher: any) {
    this.getNotification(true, refresher);
    this.events.publish('notifications:unread');
  }

  doInfinite(infiniteScroll: any) {
    setTimeout(() => {
      this.getNotification(false, infiniteScroll);
    }, 500);
  }

  getNotification(refresh: boolean, $event?) {
    if (!refresh) this.filter.page = this.filter.page + 1;
    else this.filter.page = 0;
    let clientId = this.localStorage.retrieve('clientId');;

     // Loader Spinner
     let loader = this.loadingCrtl.create({
      content: '',
      spinner: 'crescent',
    });
    loader.present();

    setTimeout(() => {
      loader.dismiss();
    }, 5000);

  this.notificationService.findByClientId(clientId, this.filter).subscribe( ( res ) => {
    this.loaded = true;
    if (refresh) this.notifications = res.body;
    else this.notifications = _.concat(this.notifications, res.body);
    loader.dismiss();
    console.log(this.notifications);
    let readDate : Date = new Date;
    readDate.setDate(readDate.getDate()-1);
    this.notificationsPrevious = this.notifications.filter((notification: Notification) => {let creationDate = new Date(notification.creationDate); return creationDate.getTime() <=  readDate.getTime()});
    this.notificationsRecent = this.notifications.filter((notification: Notification) => {let creationDate = new Date(notification.creationDate); return creationDate.getTime() > readDate.getTime()});
  }, (error) => {
    let toast = this.toastCtrl.create({ message: this.getErrorString, duration: 2000, position: 'top' });
    toast.present();
  });

  if ($event) $event.complete();
  }

  removeNotification(notification){
    this.notificationService.delete(notification.id).subscribe( ( x: any ) => {
      let removedNotification = this.notificationsPrevious.indexOf(notification);
      if(removedNotification != -1){
        this.notificationsPrevious.splice(removedNotification,1 )
      }else{
        let removedNotification = this.notificationsRecent.indexOf(notification);
        this.notificationsRecent.splice(removedNotification,1 )
      }
    })

  }
  /**
   *
   * @returns {boolean}
   */
  isAuthenticated() {
    return this.principal.isAuthenticated();
  }

}
