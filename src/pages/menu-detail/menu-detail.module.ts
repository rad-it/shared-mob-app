import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuDetailPage } from './menu-detail';

//Services
import { MenuService } from '../../providers/entities/menu.provider';
import { StayOrderService } from '../../providers/entities/stay-order.provider';

@NgModule({
  declarations: [
    MenuDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuDetailPage),
    TranslateModule.forChild()
  ],
  providers: [
    MenuService,
    StayOrderService
  ]
})
export class MenuDetailPageModule {}
