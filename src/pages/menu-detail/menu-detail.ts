import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Menu } from "../../models/entities/menu.model";

//Services
import { MenuService } from '../../providers/entities/menu.provider';
import { StayOrderService } from '../../providers/entities/stay-order.provider';
import {StayOrder} from "../../models/entities/stay-order.model";

/**
 * Generated class for the MenuDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-detail',
  templateUrl: 'menu-detail.html',
})
export class MenuDetailPage {
  menu: Menu;
  bookingId: number;
  stayId: number;
  value: number = 1;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public menuService: MenuService,
    public stayOrderService: StayOrderService) {

    this.menu = new Menu();

    this.bookingId = navParams.get('bookingId');
    this.stayId = navParams.get('stayId');

    if(this.bookingId || this.stayId) {
      this.menuService.find(navParams.get('id')).subscribe(data => {
        this.menu = data;
      });
    }
  }

  /**
   *
   */
  minus() {
    if(this.value > 0) this.value--;
  }

  /**
   *
   */
  plus() {
    this.value++;
  }

  /**
   *
   */
  addToCart() {
    let stayOrder: StayOrder = new StayOrder();

    stayOrder.bookingId = this.bookingId;
    stayOrder.stayId = this.stayId;
    stayOrder.menuId = this.menu.id;
    stayOrder.quantity = this.value;

    this.stayOrderService.create(stayOrder).subscribe(data => {
      this.viewCtrl.dismiss();
    });
  }

}
