import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { MenuAppPage } from './menu-app';

@NgModule({
  declarations: [
    MenuAppPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuAppPage),
    TranslateModule.forChild()
  ],
  exports: [
    MenuAppPage
  ]
})
export class MenuAppPageModule { }
