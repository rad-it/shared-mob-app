import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Booking } from '../../../models/entities/booking.model';
import { ClientTable } from '../../../models/entities/client-table.model';
import { BookingService } from '../../../providers/entities/booking.provider';
import { RestaurantService } from '../../../providers/entities/restaurant.provider';
import { TranslateService } from '@ngx-translate/core';
import { StayService } from '../../../providers/entities/stay.provider';
import { Restaurant } from '../../../models/entities/restaurant.model';
import { Client } from '../../../models/entities/client.model';
import { Account } from '../../../models/account.model';
import { Stay } from "../../../models/entities/stay.model";
import { LocalStorageService } from 'ngx-webstorage';
import { MainPage, RestaurantDetails } from '../../pages';
import { ChangeDetectorRef } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';
@IonicPage()
@Component({
    selector: 'page-booking-dialog',
    templateUrl: 'booking-detail.html'
})
export class BookingDetailPage {
    booking: Booking;
    bookingId: number;
    stay: Stay;
    stayId: number;
    isEditing: boolean = false;
  isReadyToSave: boolean;
    isLoading: boolean = false;
    form: FormGroup;
    restaurant: Restaurant;
    client: Client;
    account: Account;
    id: string = 'booking';
    time: string;
    currentNumber: number;
    minDate: any;
    currentDate: any;
    difference;
    getCheckString: string;

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        private localStorage: LocalStorageService,
        private bookingService: BookingService,
        private translateService: TranslateService,
        private restaurantService: RestaurantService,
        public formBuilder: FormBuilder,
        private stayService: StayService,
        private params: NavParams,
        private cdRef:ChangeDetectorRef
    ) {
        //Form
        this.initForm();

        //Init
        if(this.params.get('bookingId')) {
          this.bookingId = this.params.get('bookingId');
        }
        if(this.params.get('stayId')) {
          this.id = 'stay';
          this.stayId = this.params.get('stayId');
        }
        this.translateService.get('REQUESTED_CHECK').subscribe((value) => {
          this.getCheckString = value;
        });

        this.restaurant = new Restaurant();
        this.minDate = moment().add(1, 'hour').format();
        this.currentDate = moment().add(1, 'hour').format();
        this.currentNumber = 0;
        this.restaurant.id = params.get('restaurantId');
        this.booking = new Booking();
    }

  /**
   *
   * @param ev
   */
  save(ev, form) {
    ev.preventDefault();
    ev.stopPropagation();

    let timeBooking = form.dateTime;
    timeBooking = new Date(timeBooking).toISOString() || null;
    //Create booking
    let bookingNew =  new Booking();
    bookingNew.comments = form.comments;
    bookingNew.dateTime = timeBooking;
    bookingNew.people = form.people;
    bookingNew.restaurantId = this.restaurant.id;

    if(this.isAuthenticated()) {
      bookingNew.petitionerId = this.localStorage.retrieve('clientId');
      this.createBooking(bookingNew);
    } else {
      this.navCtrl.push('LoginPage', {booking: bookingNew});
    }

    }

  /**
   *
   */
  initForm() {

    this.form = this.formBuilder.group({
      dateTime: [{value: this.booking ? this.booking.dateTime : '', disabled: this.isEditing},  Validators.required],
      hourTime: [{value: this.booking ? this.booking.people : '', disabled: this.isEditing},  Validators.required],
      people: [{value: this.booking ? this.booking.people : 2, disabled: this.isEditing},  Validators.required],
      comments: [{value: this.booking ? this.booking.comments : '', disabled: this.isEditing}, ],
    });

  }

  onDateChange(){
  let currentDateAux = moment(this.currentDate);
    if(currentDateAux.isAfter(moment().endOf('day'))){
      this.minDate = moment().startOf('day').format();
    } else {
      this.minDate = moment().add(1, 'hour').format();
    }
     this.cdRef.detectChanges();
  }

  /**
   *
   * @param booking
   */
  createBooking(booking: Booking) {
      this.isLoading = true;
      this.bookingService.create(booking).subscribe(data => {
        this.translateService.get(['BOOKING_SEND']).subscribe(translations => {
          this.navCtrl.setRoot( MainPage , {});
          this.isLoading = false;
          let toast = this.toastCtrl.create(
            {message: translations.BOOKING_SEND , duration: 3000, position: 'top'});
          toast.present();
        });
      }, (error) => {
        this.isLoading = false;
      });
    }

  /**
   *
   */
  ionViewDidLoad() {
      this.loadData();
  }

  /**
   *
   */
  ionViewWillEnter() {

    if (this.bookingId) {
      this.bookingService.find(this.bookingId).subscribe(data => {
        this.booking = data;
        this.isEditing = true;
        let date = new Date(this.booking.dateTime);
        this.time = moment(date).format("HH:mm:ss");

        this.form.controls['people'].setValue(this.booking.people);
        this.form.controls['people'].disable();
        this.form.controls['dateTime'].setValue(moment(date).format());
        this.form.controls['dateTime'].disable();
        this.form.controls['hourTime'].setValue(this.time);
        this.form.controls['hourTime'].disable();
        this.form.controls['comments'].setValue(this.booking.comments);
        this.form.controls['comments'].disable();
      });
    }

    if (this.stayId) {
      this.stayService.find(this.stayId).subscribe(data => {
        this.stay = data;
        this.getDifferenceTime(this.stay);
      });

    }
  }

  /**
   *
   * @param $event
   */
  loadData($event?) {
        this.restaurantService.find(this.restaurant.id).subscribe(data => {
            this.restaurant = data;
            if ($event) $event.complete();
        });
    }

  /**
   * The user cancelled, dismiss without sending data back.
   */
  cancel(ev) {
      ev.preventDefault();
      ev.stopPropagation();
      this.navCtrl && this.navCtrl.pop(null, null);
  }

  /**
   * The user is done and wants to create the booking, so return it
   * back to the presenter.
   */
  done() {
      if (!this.form.valid) { return; }
      this.viewCtrl.dismiss(this.form.value);
  }

  /**
   *
   * @param error
   */
  onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'top'});
        toast.present();
    }

    /**
   *
   * @param first
   * @param second
   * @returns {boolean}
   */
  compareClientTable(first: ClientTable, second: ClientTable): boolean {
    return first && second ? first.id === second.id : first === second;
  }

  /**
   *
   * @param index
   * @param item
   * @returns {number}
   */
  trackClientTableById(index: number, item: ClientTable) {
    return item.id;
  }

  /**
   *
   * @param ev
   */
  increment(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        let people = this.form.value.people+1;
        this.form.controls['people'].setValue(people);
    }

  /**
   *
   * @param ev
   */
  decrement(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        if (this.form.value.people > 1) {
          let people = this.form.value.people-1;
          this.form.controls['people'].setValue(people);
        }
    }

  /**
   *
   * @returns {boolean}
   */
  isAuthenticated() {
    let token = this.localStorage.retrieve('authenticationToken');

    return token;
  }

  /**
   *
   * @returns {number}
   */
  getTotal() {
    let total = 0;

    if(this.booking) {
      _.each(this.booking.orders, order => {
        total += _.get(order, 'amount');
      });
    }

    return _.round(total, 2);
  }

  /**
   *
   * @param id
   */
  goToRestaurant(id) {

    let params = {id: id};
    if(this.booking.id) _.set(params, 'bookingId', this.booking.id);
    if(this.stayId) _.set(params, 'stayId', this.stayId);

    this.navCtrl.push(RestaurantDetails, params);
  }

  /**
   *
   * @param stay
   * @param index
   */
  getDifferenceTime(stay) {
    setInterval (() => {
      this.difference =
        moment.utc(moment(new Date(),"DD/MM/YYYY HH:mm:ss")
          .diff(moment(stay.init))).format("HH:mm:ss");
    }, 500);
  }

  /**
   *
   * @param stayId
   */
  check(stayId) {
    this.isLoading = true;
    this.stayService.check(stayId).subscribe(response => {
      this.stay = response;

      let toast = this.toastCtrl.create(
        {message: this.getCheckString, duration: 3000, position: 'top'});
      toast.present();
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  /**
   *
   * @returns {any}
   */
  getEntity() {
    if(this.stayId) return this.stay;
    else return this.booking
  }

  /**
   *
   * @returns {any}
   */
  getType() {
    if(this.stayId) return 'stay';
    else return 'booking';
  }

  /**
   *
   * @param stay
   * @returns {any|ClientTable|((...data:any[])=>void)|boolean}
   */
  checkStatusTable(stay) {
    return stay
      && stay.table
      && _.isEqual(stay.table.status, 'BILL_REQUESTED');
  }
}
