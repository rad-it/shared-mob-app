import { ClientTableService } from '../../entities/client-table/client-table.provider';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { BookingDetailPage } from './booking-detail';
import { BookingService } from '../../../providers/entities/booking.provider';
import { RestaurantService } from '../../../providers/entities/restaurant.provider';
import { StayOrderService } from '../../../providers/entities/stay-order.provider';
import { StayService } from '../../../providers/entities/stay.provider';
import { ComponentsModule } from '../../../components/components.module';
import { AgmCoreModule } from '@agm/core';


@NgModule({
    declarations: [
      BookingDetailPage
    ],
    imports: [
        IonicPageModule.forChild(BookingDetailPage),
        TranslateModule.forChild(),
        AgmCoreModule,
        ComponentsModule
    ],
    exports: [
      BookingDetailPage
    ],
    providers: [
        BookingService,
        ClientTableService,
        RestaurantService,
        StayOrderService,
        StayService
    ]
})
export class BookingDialogPageModule {}
