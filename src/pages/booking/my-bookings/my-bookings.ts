import { Component, OnInit } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController
} from 'ionic-angular';
import { Booking } from "../../../models/entities/booking.model";
import { Stay } from "../../../models/entities/stay.model";
import { BookingDialog } from "../../pages";
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { BookingDetail, Tab1Root } from "../../pages";

//Services
import { TranslateService } from '@ngx-translate/core';
import { BookingService } from '../../../providers/entities/booking.provider';
import { StayService } from '../../../providers/entities/stay.provider';
import { LocalStorageService } from 'ngx-webstorage';

import * as _ from 'lodash';
/**
 * Generated class for the MyBookingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-bookings',
  templateUrl: 'my-bookings.html',
})
export class MyBookingsPage implements OnInit {
  activeBookings: Booking[] = [];
  pendingBookings: Booking[] = [];
  stays: Stay[] = [];
  filter: any = {};
  getErrorString:string = '';
  loaded: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private localStorage: LocalStorageService,
    public loadingCrtl: LoadingController,
    private translateService: TranslateService,
    private bookingService: BookingService,
    private stayService: StayService,
    private qrScanner: QRScanner) {

    this.filter.page = 0;
    this.filter.size = 3;

    this.translateService.get('GET_ERROR').subscribe((value) => {
      this.getErrorString = value;
    });
  }

  ngOnInit() {
    this.getBookings(true);
  }

   /**
   *
   */
  ionViewWillEnter() {
    this.getBookings(true);
  }

  /**
   * Api call to load more products when infinite scroll.
   * @param infiniteScroll
   */
  doInfinite(infiniteScroll: any) {
    setTimeout(() => {
      this.getBookings(false, infiniteScroll);
    }, 500);
  }

  /**
   * Api call to refresh products
   * @param refresher
   */
  doRefresh(refresher: any) {
    this.getBookings(true, refresher);
  }

   detail() {
     this.navCtrl.push('RestaurantDetailPage');
 }

  scan() {
    this.navCtrl.push('QrStayPage');
}

  /**
   *
   * @param $event
   * @param refresh
   */
  getBookings(refresh: boolean, $event?) {
    if (!refresh) this.filter.page = this.filter.page + 1;
    else this.filter.page = 0;
    let clientId = this.localStorage.retrieve('clientId');;

    // Loader Spinner
    let loader = this.loadingCrtl.create({
      content: '',
      spinner: 'crescent',
    });
    loader.present();

    setTimeout(() => {
      loader.dismiss();
    }, 5000);

    //Stays
    this.stayService.findByClientId(clientId)
      .subscribe( res =>  {
        this.stays = res;
        loader.dismiss();
      });

    this.filter.status = 'ACTIVE';
    this.bookingService.filterByClient(clientId, this.filter)
      .subscribe(res => {
        if (refresh) this.activeBookings = res.body;
        else this.activeBookings = _.concat(this.activeBookings, res.body);
        loader.dismiss();
      });

    this.filter.status = 'PENDING';
    this.bookingService.filterByClient(clientId, this.filter)
      .subscribe( res =>  {
          this.loaded = true;
          if (refresh) this.pendingBookings = res.body;
          else this.pendingBookings = _.concat(this.pendingBookings, res.body);
          loader.dismiss();
        },
        (error) => {
          let toast = this.toastCtrl.create({ message: this.getErrorString, duration: 2000, position: 'top' });
          toast.present();
        });

    if ($event) $event.complete();
  }

  /**
   *
   * @param qty
   * @returns {any}
   */
  getQty(qty) {
    if (qty == 1) return 'MY_BOOKINGS_PERSON';
    else return 'MY_BOOKINGS_PEOPLE';
  }

  /**
   *
   * @param booking
   */
  goToBookingProfile(booking) {
    this.navCtrl.push(BookingDetail, {restaurantId: booking.restaurant.id, bookingId: booking.id});
  }

  /**
   *
   */
  goToHome() {
    this.navCtrl.setRoot(Tab1Root);
  }

}
