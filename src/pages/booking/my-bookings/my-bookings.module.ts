import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingsPage } from './my-bookings';
import { ComponentsModule } from '../../../components/components.module';
import { QrStayPage } from '../../stay/qrstay/qr-stay';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

//Services
import { BookingService } from '../../../providers/entities/booking.provider';
import { StayService } from '../../../providers/entities/stay.provider';

@NgModule({
  declarations: [
    MyBookingsPage,
        // QrStayPage,
  ],
  imports: [
        // IonicPageModule.forChild(QrStayPage),
    IonicPageModule.forChild(MyBookingsPage),
    TranslateModule.forChild(),
    ComponentsModule
  ],
   exports: [
        // QrStayPage
   ],
  providers: [
    BookingService,
    StayService,
    QRScanner
  ]
})
export class MyBookingsPageModule {}
