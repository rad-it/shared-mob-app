import { BaseEntity } from '../../models';

export class MeasuringUnit implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public code?: string,
    ) {
    }
}
