import { BaseEntity } from '../../models';

export class MealClassification implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
