import { BaseEntity } from '../../models';
import { RestaurantType } from './restaurant-type.model';
// import { Location } from '../../models'

export class Restaurant implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public cuit?: string,
        public cbu?: string,
        public locationId?: number,
        public location?: any,
        public pictures?: BaseEntity[],
        public roles?: BaseEntity[],
        public sectors?: BaseEntity[],
        public distance?: number,
        public restaurantTypeId?: number,
        public restaurantType?: RestaurantType,
        public mealClassifications?: BaseEntity[],
    ) {
    }
}
