import { BaseEntity } from '../../models';

export class Picture implements BaseEntity {
    constructor(
        public id?: number,
        public contentType?: string,
        public content?: any,
        public restaurantId?: number,
    ) {
    }
}
