import { BaseEntity } from '../../models';
import { Picture } from './picture.model';

export class Client implements BaseEntity {
    constructor(
        public id?: number,
        public birthdate?: any,
        public userId?: number,
        public locationId?: number,
        public nationalityId?: number,
        public telephoneNumber?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public picture?: Picture,
        public pictureId?: number,
        public location?: {
            latitude?: number,
            longitude?: number,
        }
    ) {
    }
}
