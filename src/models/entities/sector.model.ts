import { BaseEntity } from '../../models';

export class Sector implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public accesibility?: boolean,
        public deleteDate?: any,
        public restaurantId?: number,
        public tables?: BaseEntity[],
        public locationId?: number,
    ) {
        this.accesibility = false;
    }
}
