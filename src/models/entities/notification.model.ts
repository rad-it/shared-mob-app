import { BaseEntity } from '../../models';
import { Restaurant } from '../../pages/entities/restaurant';

export class Notification {
    constructor(
        public restaurant?: Restaurant,
        public creationDate?: Date,
        public readDate?: Date,
        public type?: String,
        public parameters?: String[],
    ) {
    }
}