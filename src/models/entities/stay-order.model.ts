import { BaseEntity } from '../../models';
import { Menu } from './menu.model';

export class StayOrder implements BaseEntity {
    constructor(
        public id?: number,
        public quantity?: number,
        public released?: any,
        public price?: number,
        public bookingId?: number,
        public stayId?: number,
        public menuId?: number,
        public menu?: Menu
    ) {
    }
}
