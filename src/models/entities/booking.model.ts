import { BaseEntity } from '../../models';
import { ClientTable } from './client-table.model';

export const enum BookingStatus {
    'PENDING',
    'REJECTED',
    'ACTIVE',
    'DISMISSED',
    'FINALIZED'
}

export class Booking implements BaseEntity {
    constructor(
        public id?: number,
        public dateTime?: any,
        public people?: number,
        public clientName?: string,
        public clientTelephoneNumber?: string,
        public status?: string,
        public comments?: string,
        public orders?: BaseEntity[],
        public tableId?: number,
        public table?: ClientTable,
        public restaurantId?: number,
        public petitionerId?: number
    ) {
    }
}
