import { BaseEntity } from '../../models';
import { MenuClassification } from './menu-classification.model';
import { MeasuringUnit } from './measuring-unit.model';
import { Restaurant } from './restaurant.model';


export const enum DestinationType {
    'BAR',
    'COOKER'
}

export class Menu implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public price?: number,
        public quantity?: number,
        public destination?: DestinationType,
        public disabled?: boolean,
        public deleteDate?: any,
        public measuringUnitId?: number,
        public measuringUnit?: MeasuringUnit,
        public classificationId?: number,
        public classification?: MenuClassification,
        public restaurantId?: number,
        public restaurant?: Restaurant,
        public pictures?: BaseEntity[],
        public subclassifications?: BaseEntity[],
    ) {
        this.disabled = false;
    }
}
