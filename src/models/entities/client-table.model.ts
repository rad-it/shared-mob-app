import { BaseEntity } from '../../models';
import { Sector } from './sector.model';

export class ClientTable implements BaseEntity {
    constructor(
        public id?: number,
        public number?: string,
        public chairs?: number,
        public modifiable?: boolean,
        public deleteDate?: any,
        public sectorId?: number,
        public statusId?: number,
        public sector?: Sector
    ) {
        this.modifiable = false;
    }
}
