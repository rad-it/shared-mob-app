import { BaseEntity } from '../../models';
import { Booking } from './booking.model';
import { ClientTable } from './client-table.model';

export class Stay implements BaseEntity {
    constructor(
        public id?: number,
        public amount?: number,
        public people?: number,
        public init?: any,
        public end?: any,
        public discount?: number,
        public bookingId?: number,
        public orders?: BaseEntity[],
        public tableId?: number,
        public booking?: Booking,
        public table?: ClientTable
    ) {
    }
}
