import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';
import { Principal } from '../providers/auth/principal.service';
import { MainPage } from '../pages/pages';
import { Settings } from '../providers/providers';
import { FCM } from '@ionic-native/fcm';

import { FcmProvider } from '../providers/fcm/fcm'

import { ToastController } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';
import { tap } from 'rxjs/operators';
import { Events } from 'ionic-angular';

@Component({
  template: `<ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage;
  state: any;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Welcome', component: 'WelcomePage' },
    { title: 'Tabs', component: 'TabsPage' },
    { title: 'Login', component: 'LoginPage' },
    { title: 'Signup', component: 'SignupPage' },
    { title: 'Menu', component: 'MenuPage' },
    { title: 'Settings', component: 'SettingsPage' },
    { title: 'Entities', component: 'EntityPage' },
    { title: 'Notifitation', component: 'NotificationPage'}
  ];

  constructor(private translate: TranslateService, platform: Platform, settings: Settings, private config: Config,  toastCtrl: ToastController,
              private statusBar: StatusBar, private splashScreen: SplashScreen, private principal: Principal,private fcm: FcmProvider, public events: Events) {
    platform.ready().then(() => {

      this.principal.getAuthenticationState().subscribe( (state: any )  =>{
          this.state = state;

          if( this.state != null){
            console.log('state', this.state);
            fcm.getToken();

            // Listen to incoming messages
            fcm.listenToNotifications().pipe(
              tap(msg => {
                 this.events.publish('notifications:unread');

                // show a toast
                const toast = toastCtrl.create({
                  message: msg.title,
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
              })
            )
            .subscribe()
         statusBar.styleDefault();
         splashScreen.hide();
           }
      });
      this.principal.identity();

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

    });

    this.rootPage = MainPage;

    this.initTranslate();
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  /**
   *
   * @returns {boolean}
   */
  isAuthenticated() {
    return this.principal.isAuthenticated();
  }
}
