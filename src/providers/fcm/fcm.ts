import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { LocalStorageService } from 'ngx-webstorage';
import { Api } from '../api/api';

/*
  Generated class for the FcmProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FcmProvider {

  constructor(public http: HttpClient,
    public firebaseNative: Firebase,
    public afs: AngularFirestore,
    private localStorage: LocalStorageService,
    private platform: Platform,
    ) {
  }

  private baseResourceUrl = Api.API_URL.replace('api', 'ichooseapi/api');
  private resourceUrl = this.baseResourceUrl + '/firebase/fcm/token';

  // Get permission from the user
  async getToken() {
    let token;

    if (this.platform.is('android')) {

      token = await this.firebaseNative.getToken().then((token) => {
        this.updateToken(token)
      })
      await this.firebaseNative.grantPermission();
    }

    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken().then(function(token){
        this.updateToken(token)
    });
      await this.firebaseNative.grantPermission();
    }

    if (!this.platform.is('cordova')) {
      token = await this.firebaseNative.getToken().then(function(token){
        this.updateToken(token)
    });
      await this.firebaseNative.grantPermission();
    }

    this.firebaseNative.onTokenRefresh()
 .subscribe((token: string) => { this.updateToken(token)  });

    return this.saveTokenToFirestore(token)
  }

  private updateToken(token){
    const body = { fcmToken: token };
    this.http.put<any>(this.resourceUrl, body, { observe: 'response' }).subscribe(result => console.log(result), err => console.log(err));
 }

  // Save the token to firestore
  private saveTokenToFirestore(token) {
    if (!token) return;

    const devicesRef = this.afs.collection('devices')

    const docData = {
      token,
      userId: 'testUser',
    }

    return devicesRef.doc(token).set(docData)
   }

  // Listen to incoming FCM messages
  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen();
   }

}
