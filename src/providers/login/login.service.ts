import { Injectable } from '@angular/core';
import { ClientService } from '../entities/client.provider';
import { LocalStorageService } from 'ngx-webstorage';
//import { JhiLanguageService } from 'ng-jhipster';

import { Principal } from '../auth/principal.service';
import { AuthServerProvider } from '../auth/auth-jwt.service';
import { TranslateService } from '@ngx-translate/core';
import { Events } from 'ionic-angular';


@Injectable()
export class LoginService {

    constructor(
        private principal: Principal,
        private authServerProvider: AuthServerProvider,
        private translate: TranslateService,
        private clientService: ClientService,
        private $localStorage: LocalStorageService,
        public events: Events,
    ) {}

    login(credentials, callback?) {
        const cb = callback || function() {};

        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe((data) => {
                this.principal.identity(true).then((account) => {
                  // After the login the language will be changed to
                  // the language selected by the user during his registration
                  this.clientService.findByUser(5).subscribe(
                    (clientData) => {
                      this.$localStorage.store('clientId', clientData.id);
                      this.events.publish('notifications:unread');
                    });

                  if (account !== null) {
                      this.translate.use(account.langKey);
                  }
                  resolve(data);
                });
                return cb();
            }, (err) => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }

    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    }

    logout() {
        this.authServerProvider.logout().subscribe();
        this.events.publish('notifications:unread');
        this.principal.authenticate(null);
    }
}
