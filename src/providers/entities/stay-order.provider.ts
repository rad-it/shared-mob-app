import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../api/api';
// todo: handle dates

import { StayOrder } from '../../models/entities/stay-order.model';

@Injectable()
export class StayOrderService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/stay-orders';

    constructor(private http: HttpClient) { }

    create(stayOrder: StayOrder): Observable<StayOrder> {
        return this.http.post(this.resourceUrl, stayOrder);
    }

    update(stayOrder: StayOrder): Observable<StayOrder> {
        return this.http.put(this.resourceUrl, stayOrder);
    }

    find(id: number): Observable<StayOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
