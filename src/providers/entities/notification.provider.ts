import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../api/api';
import { Notification } from './../../models/entities/notification.model';
import * as _ from 'lodash';

@Injectable()
export class NotificationService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/notifications';

    constructor(private http: HttpClient) { }

    findByClientId(id: number, filter: any): Observable<any> {
         let params = {};
         _.set(params, 'page', filter.page.toString());
         _.set(params, 'size', filter.size.toString());
         _.set(params, 'sort', ['id,desc']);
        let url = Api.API_URL.replace('api', 'ichooseapi/api') 
        return this.http.get(`${url}/clients/${id}/notifications` ,{
            params: params,
            observe: 'response'});
    }

    getCountOfUnreadNotifications(clientId: number) : Observable<any> {
        let url = `${Api.API_URL.replace('api', 'ichooseapi/api')}/clients/${clientId}/notifications/unread`;
        return this.http.get(url, { observe: 'response'});
    }

    markAsRead(id: number) : Observable<any> {
       return this.http.put<any>(this.resourceUrl + `/${id}/read`, { observe: 'response' });
    }

    delete(id: number) : Observable<any> {
        return this.http.delete(this.resourceUrl + `/${id}`, { observe: 'response', responseType: 'text' });
    }
}
