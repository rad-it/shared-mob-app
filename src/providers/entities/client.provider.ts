import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../api/api';
// todo: handle dates

import { Client } from '../../models/entities/client.model';

@Injectable()
export class ClientService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/clients';
    private resourceUrlUsers = Api.API_URL.replace('api', 'ichooseapi/api') + '/users';

    constructor(private http: HttpClient) { }

    create(client: Client): Observable<Client> {
        return this.http.post(this.resourceUrl, client);
    }

    update(client: Client): Observable<Client> {
        return this.http.put(this.resourceUrl, client);
    }

    find(id: number): Observable<Client> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    findByUser(id: number): Observable<any> {
        return this.http.get(`${this.resourceUrlUsers}/${id}/client`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
