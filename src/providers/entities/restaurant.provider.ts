import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../api/api';

import { Restaurant } from '../../models/entities/restaurant.model';
import * as _ from 'lodash';

@Injectable()
export class RestaurantService {
  private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/restaurants';
  private resourceFilterUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/_filter/restaurants';

    constructor(private http: HttpClient) { }

  /**
   *
   * @param restaurant
   * @returns {Observable<Object>}
   */
    create(restaurant: Restaurant): Observable<Restaurant> {
        return this.http.post(this.resourceUrl, restaurant);
    }

  /**
   *
   * @param restaurant
   * @returns {Observable<Object>}
   */
    update(restaurant: Restaurant): Observable<Restaurant> {
        return this.http.put(this.resourceUrl, restaurant);
    }

  /**
   *
   * @param id
   * @returns {Observable<Object>}
   */
    find(id: number): Observable<Restaurant> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

  /**
   *
   * @param req
   * @returns {Observable<Object>}
   */
    query(filter: any): Observable<any> {

      let params = {};
      _.set(params, 'name', filter.search);
      _.set(params, 'page', filter.page.toString());
      _.set(params, 'size', filter.size.toString());
      if (filter.lat) _.set(params, 'lat', filter.lat.toString());
      if (filter.lng) _.set(params, 'lon', filter.lng.toString());

      if (filter.mealClassifications && filter.mealClassifications.length>0){
        let mealClassifications = '';
        _.each(filter.mealClassifications, (meal,index) => {
            mealClassifications = mealClassifications.concat(meal);
            if (!_.isEqual(filter.mealClassifications.length-1,index))  mealClassifications = mealClassifications.concat(',');
        });
        _.set(params, 'mealClassifications', mealClassifications);
      }

      return this.http.get(this.resourceFilterUrl, {
        params: params,
        observe: 'response'});
    }

  /**
   *
   * @param id
   * @returns {Observable<HttpResponse<string>>}
   */
    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }

  /**
   *
   * @param id
   * @returns {Observable<Object>}
   */
    getMenus(id: number): Observable<any> {
      return this.http.get(`${this.resourceUrl}/${id}/menus`);
    }

  /**
   *
   * @param id
   * @returns {Observable<Object>}
   */
    getPromotions(id: number): Observable<any> {
      return this.http.get(`${this.resourceUrl}/${id}/promotions`);
    }
}
