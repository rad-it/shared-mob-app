import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../api/api';
// todo: handle dates

import { Stay } from './../../models/entities/stay.model';

@Injectable()
export class StayService {
    private baseUrl = Api.API_URL.replace('api', 'ichooseapi/api');
    private resourceUrl = this.baseUrl + '/stays';
    constructor(private http: HttpClient) { }

    create(stay: Stay): Observable<Stay> {
        return this.http.post(this.resourceUrl, stay);
    }

    createByClientId(clientId: number,body:any): Observable<Stay> {
        return this.http.post(`${this.baseUrl}/clients/${clientId}/stays/`, body);
    }
    update(stay: Stay): Observable<Stay> {
        return this.http.put(this.resourceUrl, stay);
    }

    find(id: number): Observable<Stay> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    findByClientId(clientId: number): Observable<any> {
      return this.http.get(`${this.baseUrl}/clients/${clientId}/stays/current`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }

    check(id: number): Observable<any> {
      return this.http.put(`${this.resourceUrl}/${id}/request-bill`, null);
    }
}