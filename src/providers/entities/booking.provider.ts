import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../api/api';
// todo: handle dates

import { Booking } from '../../models/entities/booking.model';
import * as _ from 'lodash';

@Injectable()
export class BookingService {
    private baseResourceUrl = Api.API_URL.replace('api', 'ichooseapi/api');
    private resourceUrl = this.baseResourceUrl + '/bookings';

    constructor(private http: HttpClient) { }

  /**
   *
   * @param booking
   * @returns {Observable<Object>}
   */
    create(booking: Booking): Observable<Booking> {
        return this.http.post(this.resourceUrl, booking);
    }

  /**
   *
   * @param booking
   * @returns {Observable<Object>}
   */
    update(booking: Booking): Observable<Booking> {
        return this.http.put(this.resourceUrl, booking);
    }

  /**
   *
   * @param id
   * @returns {Observable<Object>}
   */
    find(id: number): Observable<Booking> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

  /**
   *
   * @param req
   * @returns {Observable<Object>}
   */
    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

  /**
   *
   * @param id
   * @returns {Observable<HttpResponse<string>>}
   */
    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }

  /**
   *
   * @param clientId
   * @returns {Observable<Object>}
   */
    filterByClient(clientId: number, filter: any): Observable<any> {

    let params = {};
    _.set(params, 'status', filter.status);
    _.set(params, 'page', filter.page.toString());
    _.set(params, 'size', filter.size.toString());

      return this.http.get(`${this.baseResourceUrl}/clients/${clientId}/bookings`, {
        params: params,
        observe: 'response'});
    }
}
