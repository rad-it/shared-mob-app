import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../api/api';
import { Menu } from './../../models/entities/menu.model';

@Injectable()
export class MenuService {
    private resourceUrl = Api.API_URL.replace('api', 'ichooseapi/api') + '/menus';

    constructor(private http: HttpClient) { }

    create(menu: Menu): Observable<Menu> {
        return this.http.post(this.resourceUrl, menu);
    }

    update(menu: Menu): Observable<Menu> {
        return this.http.put(this.resourceUrl, menu);
    }

    find(id: number): Observable<Menu> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
