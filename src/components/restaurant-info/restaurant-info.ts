import { Component, Input } from '@angular/core';
import { Restaurant } from "../../models/entities/restaurant.model";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the RestaurantInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'restaurant-info',
  templateUrl: 'restaurant-info.html'
})
export class RestaurantInfoComponent {
  zoom:number = 15;
  lat: any;
  long: any;
  @Input() restaurant: Restaurant;

  constructor(private launchNavigator: LaunchNavigator,
              private geolocation: Geolocation) {
              }

  navigator(){
    this.lat = this.restaurant.location.latitude;
    this.long = this.restaurant.location.longitude;
    let coord = [this.lat, this.long];
    this.launchNavigator.navigate(coord)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      )
  }

}
