import { Component, Input } from '@angular/core';
import { Booking } from "../../models/entities/booking.model";

/**
 * Generated class for the PromotionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'promotions',
  templateUrl: 'promotions.html'
})
export class PromotionsComponent {
  @Input('promotions') promotions: any[] = [];
  @Input('booking') booking: Booking;
  constructor() {}

}
