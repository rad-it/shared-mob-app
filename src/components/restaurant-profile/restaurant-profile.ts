import { Component, Input } from '@angular/core';
import {Restaurant} from "../../models/entities/restaurant.model";

/**
 * Generated class for the RestaurantProfileComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'restaurant-profile',
  templateUrl: 'restaurant-profile.html'
})
export class RestaurantProfileComponent {
  @Input() restaurant = Restaurant;

  constructor() {
  }

}
