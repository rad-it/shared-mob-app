import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { NavController } from 'ionic-angular';

//Model
import { Menu } from '../../models/entities/menu.model';
import { Booking } from "../../models/entities/booking.model";
import { Stay } from "../../models/entities/stay.model";

import { MenuDetail } from "../../pages/pages";
import * as _ from 'lodash';

/**
 * Generated class for the MenusComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'menus',
  templateUrl: 'menus.html'
})
export class MenusComponent implements OnInit, OnChanges {
  @Input('menus') menus: Menu[] = [];
  @Input('booking') booking: Booking;
  @Input('stay') stay: Stay;
  classifications: any[] = [];

  constructor(public navCtrl: NavController) {}

  ngOnInit() {}

  /**
   *
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if(changes.menus) {
      this.menus = changes.menus.currentValue;
      this.getCategories();
    }
  }

  /**
   *
   */
  getCategories() {
    this.classifications = [];
    _.each(this.menus, menu => {
      if (menu.classification) {
        let classification = _.find(this.classifications, {'id': menu.classification.id});

        if (!classification) {
          this.classifications.push({
            id: menu.classification.id,
            name: menu.classification.name,
            items: [menu]
          });
        } else {

          _.each(this.classifications, clas => {
            if (_.isEqual(clas.id, menu.classification.id)) {
              clas.items.push(menu);
            }
          });

        }

      }
    });
  }

  /**
   *
   * @param menu
   */
  viewMenu(menu) {
    if(this.booking) this.navCtrl.push(MenuDetail, {id: menu.id, bookingId: this.booking.id});
    else if(this.stay) this.navCtrl.push(MenuDetail, {id: menu.id, stayId: this.stay.id});
  }

}
