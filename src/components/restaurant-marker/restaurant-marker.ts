import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestaurantDetails } from '../../pages/pages';
/**
 * Generated class for the RestaurantMarkerComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'restaurant-marker',
  templateUrl: 'restaurant-marker.html'
})
export class RestaurantMarkerComponent {
  @Input() restaurant;
  @Output() close = new EventEmitter<any>();

  constructor(private navCtrl: NavController) {}

  /**
   *
   */
  onClose() {
    this.close.emit(null);
  }

  /**
   *
   * @param restaurant
   */
  goToRestaurantProfile(restaurant) {
    this.navCtrl.push(RestaurantDetails, {id: restaurant.id});
  }

}
