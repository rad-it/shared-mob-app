import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Booking } from "../../models/entities/booking.model";

//Models
import { BookingDetail } from "../../pages/pages";
import * as moment from 'moment';
import * as _ from 'lodash';
/**
 * Generated class for the BookingItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'booking-item',
  templateUrl: 'booking-item.html'
})
export class BookingItemComponent implements OnInit{
  @Input() bookings: Booking[];
  @Input() stay: boolean;
  differences;

  constructor(public navCtrl: NavController) {

  }

  ngOnInit() {}

  /**
   *
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if(changes.bookings && this.stay) {
      this.differences = new Array(this.bookings.length);
      _.each(this.bookings, (stay,index) => {
        this.getDifferenceTime(stay, index);
      });
    }
  }

  /**
   *
   * @param qty
   * @returns {any}
   */
  getQty(qty) {
    if(qty==1) return 'MY_BOOKINGS_PERSON';
    else return 'MY_BOOKINGS_PEOPLE';
  }

  /**
   *
   * @param booking
   */
  goToBookingProfile(booking) {
    if(!this.stay) {
      this.navCtrl.push(BookingDetail, {restaurantId: booking.restaurant.id, bookingId: booking.id});
    } else {
      this.navCtrl.push(BookingDetail, {restaurantId: booking.restaurant.id, stayId: booking.id});
    }
  }

  /**
   *
   * @param status
   * @returns {string}
   */
  getStatus(status) {
    if(status) return 'STATUS_'+status;
  }

  /**
   *
   * @param stay
   * @param index
   */
  getDifferenceTime(stay, index) {
    setInterval (() => {
      this.differences[index] =
        moment.utc(moment(new Date(),"DD/MM/YYYY HH:mm:ss")
          .diff(moment(stay.init))).format("HH:mm:ss");
    }, 500);
  }

}
