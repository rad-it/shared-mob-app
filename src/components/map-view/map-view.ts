import { Component,
  EventEmitter,
  Input,
  Output,
  OnInit } from '@angular/core';
import { Restaurant } from "../../models/entities/restaurant.model";
/**
 * Generated class for the MapViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'map-view',
  templateUrl: 'map-view.html'
})
export class MapViewComponent implements OnInit{
  zoom:number;
  @Input() lat: any;
  @Input() lng: any;
  @Input() restaurants: Restaurant[];
  @Output() restaurantSelected = new EventEmitter<Restaurant>();

  constructor() {}

  ngOnInit() {

    if(this.lat || this.lng) {
      this.zoom = 15;
    } else {
      this.lat = -35.0199554;
      this.lng = -64.9668381;
      this.zoom = 5;
    }
  }

  /**
   *
   * @param restaurant
   */
  markerClick(restaurant) {
    this.restaurantSelected.emit(restaurant);
  }
}
