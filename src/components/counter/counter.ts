import { Component, Input } from '@angular/core';
import { Booking } from "../../models/entities/booking.model";

/**
 * Generated class for the CounterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'counter',
  templateUrl: 'counter.html'
})
export class CounterComponent {
  @Input() value: number;

  constructor() {

  }

}
