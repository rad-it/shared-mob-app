import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MenusComponent } from './menus/menus';
import { PromotionsComponent } from './promotions/promotions';
import { RestaurantProfileComponent } from './restaurant-profile/restaurant-profile';
import { CounterComponent } from './counter/counter';
import { RestaurantInfoComponent } from './restaurant-info/restaurant-info';
import { AgmCoreModule } from '@agm/core';
import { BookingItemComponent } from './booking-item/booking-item';
import { MapViewComponent } from './map-view/map-view';
import { RestaurantMarkerComponent } from './restaurant-marker/restaurant-marker';
import { OrdersComponent } from './orders/orders';
import { StayOrderService } from '../providers/entities/stay-order.provider';

@NgModule({
	declarations: [
	  MenusComponent,
    PromotionsComponent,
    RestaurantProfileComponent,
    CounterComponent,
    RestaurantInfoComponent,
    BookingItemComponent,
    MapViewComponent,
    RestaurantMarkerComponent,
    OrdersComponent,

  ],
	imports: [
    IonicPageModule,
    TranslateModule.forChild(),
    AgmCoreModule
  ],
	exports: [
	  MenusComponent,
    PromotionsComponent,
    RestaurantProfileComponent,
    CounterComponent,
    RestaurantInfoComponent,
    BookingItemComponent,
    MapViewComponent,
    RestaurantMarkerComponent,
    OrdersComponent
  ],
  providers: [StayOrderService]
})
export class ComponentsModule {}
