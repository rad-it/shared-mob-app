import { Component, Input } from '@angular/core';
import { StayOrderService } from '../../providers/entities/stay-order.provider';
import * as _ from 'lodash';
/**
 * Generated class for the OrdersComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'orders',
  templateUrl: 'orders.html'
})
export class OrdersComponent {
  @Input() entity: any;
  @Input() type: string;

  constructor(private stayOrderService: StayOrderService) {}

  /**
   * For Bookings
   * @param id
   */
  removeMenu(id) {
    this.stayOrderService.delete(id).subscribe(response => {
      _.remove(this.entity.orders, {id: id});
    });
  }

  /**
   *
   * @param stay
   * @returns {any|ClientTable|((...data:any[])=>void)|boolean}
   */
  checkStatusTable(stay) {
    return stay
      && stay.table
      && _.isEqual(stay.table.status, 'BILL_REQUESTED');
  }
}
